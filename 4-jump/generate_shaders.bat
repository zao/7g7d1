@mkdir data\shaders\dx11 2>NUL
shaderc -I shaders --platform windows --varyingdef shaders\single_def.sc --profile vs_5_0 --type vertex -f shaders\debug_vs.sc -o data\shaders\dx11\debug_vs.bin
shaderc -I shaders --platform windows --varyingdef shaders\single_def.sc --profile ps_5_0 --type frag   -f shaders\debug_fs.sc -o data\shaders\dx11\debug_fs.bin

shaderc -I shaders --platform windows --varyingdef shaders\single_def.sc --profile vs_5_0 --type vertex -f shaders\single_vs.sc -o data\shaders\dx11\single_vs.bin
shaderc -I shaders --platform windows --varyingdef shaders\single_def.sc --profile ps_5_0 --type frag   -f shaders\single_fs.sc -o data\shaders\dx11\single_fs.bin
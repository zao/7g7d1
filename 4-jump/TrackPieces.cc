#include "TrackPieces.h"
#include <assert.h>

namespace TrackPieces {
	static Mesh meshes[Piece::Count];

	static Mesh BuildMesh(size_t vertexCount, Vertex7GM const* vertices) {
		Mesh ret;
		uint32_t const VertexByteCount = (uint32_t)(vertexCount * sizeof(Vertex7GM));
		bgfx::Memory const* mem = bgfx::copy(vertices, VertexByteCount);

		ret.vertexCount = (uint32_t)vertexCount;
		ret.vbh = bgfx::createVertexBuffer(mem, Vertex7GM::decl);
		return ret;
	}

	void Init() {
#if 0
		// We only need top/front/left/right sides
		float const W = 10.0f;
		float const H = 5.0f;
		float const D = 30.0f;
		float3 const
			xN = -W/2.0f * float3::unitX,
			xP = +W/2.0f * float3::unitX,
			yN = -H/2.0f * float3::unitY,
			yM = +H/4.0f * float3::unitY,
		   	yP = +H/2.0f * float3::unitY,
			zN = +D/2.0f * float3::unitZ,
			zP = -D/2.0f * float3::unitZ;
		//	6===7
		// 2===3|
		// ||  ||
		// |a==|b
		// 8===9|
		// ||  ||
		// |4==|5
		// 0===1
		float3 const positions[] = {
			xN + yN + zN, xP + yN + zN, // 0 1
			xN + yP + zN, xP + yP + zN, // 2 3
			xN + yN + zP, xP + yN + zP, // 4 5
			xN + yP + zP, xP + yP + zP, // 6 7
			xN + yM + zN, xP + yM + zN, // 8 9
			xN + yM + zP, xP + yM + zP, // a b
		};
		float3 const normals[] = {
			+float3::unitZ,
			+float3::unitY,
			-float3::unitX,
			+float3::unitX,
		};
		float2 const tc[] = { float2::zero };
		uint32_t const color[] = { 0xFFFFFFFF };
		auto const V = [&](size_t iP, size_t iN, size_t iT, size_t iC) -> Vertex7GM {
			return Vertex7GM::Make(positions[iP], normals[iN], tc[iT], color[iC]);
		};
		Vertex7GM vtx[] = {
			V(0, 0, 0, 0), V(1, 0, 0, 0), V(2, 0, 0, 0), // front
			V(2, 0, 0, 0), V(1, 0, 0, 0), V(3, 0, 0, 0),
			V(3, 1, 0, 0), V(7, 1, 0, 0), V(2, 1, 0, 0), // top
			V(2, 1, 0, 0), V(7, 1, 0, 0), V(6, 1, 0, 0),
			V(0, 2, 0, 0), V(2, 2, 0, 0), V(4, 2, 0, 0), // left
			V(4, 2, 0, 0), V(2, 2, 0, 0), V(6, 2, 0, 0),
			V(5, 3, 0, 0), V(7, 3, 0, 0), V(1, 3, 0, 0), // right
			V(1, 4, 0, 0), V(7, 4, 0, 0), V(3, 4, 0, 0),
		};
		meshes[Piece::GroundSlab] = BuildMesh(24, vtx);
#endif
		meshes[Piece::GroundSlab] = Load7GM("data/meshes/ground-slab.7gm");
		meshes[Piece::Block] = Load7GM("data/meshes/block.7gm");
		meshes[Piece::FreeTunnel] = Load7GM("data/meshes/free-tunnel.7gm");
		meshes[Piece::BlockTunnel] = Load7GM("data/meshes/block-tunnel.7gm");
	}

	Mesh GetMesh(Piece::Type type) {
		assert(type < Piece::Count);
		return meshes[type];
	}
}

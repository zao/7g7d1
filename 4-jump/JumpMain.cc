#ifndef NOMINMAX
#define NOMINMAX
#endif
#include "SDL2/SDL.h"
#include "SDL2/SDL_syswm.h"
#ifdef __APPLE__
#include "SDL2_mixer/SDL_mixer.h"
#else
#include "SDL2/SDL_mixer.h"
#endif
#ifdef None
#undef None
#endif
#include <bgfx.h>
#include <bgfxplatform.h>
#include <bx/timer.h>
#include <MathGeoLib/MathGeoLib.h>

#include <algorithm>
#include <deque>
#include <functional>
#include <iostream>
#include <map>
#include <vector>

#include "Utils.h"
#include "Tuning.h"
#include "Formats.h"
#include "TrackPieces.h"
#include "OS.h"
#include "Board.h"

int ZMain();

#ifdef WIN32
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	return ZMain();
}
#else
void OutputDebugStringA(char const* s) {
	std::cerr << s;
}

#include <unistd.h>
int main() {
	std::string rootPath = GetApplicationRoot();
	if (rootPath.size()) {
		chdir (rootPath.c_str());
	}
	return ZMain();
}
#endif

struct ShipContacts {
	int GetSegmentCount() const { return 2; }
	Board::LineSegment GetSegment(int index, Board::Coordinate origin) const {
		assert(index >= 0 && index < GetSegmentCount());
		Board::LineSegment ret;
		ret[0] = GetPoint(1*4 + 2*index + 0, origin);
		ret[1] = GetPoint(1*4 + 2*index + 1, origin);
		return ret;
	}

	int GetPointCount() const { return 8; }
	Board::Coordinate GetPoint(int index, Board::Coordinate origin) const {
		assert(index >= 0 && index < GetPointCount());
		Board::DimensionX dXs[] = {
			Board::DimensionX::Make(0, 0),
			Board::DimensionX::Make(0, tuning::ShipWidth-1),
		};
		Board::DimensionY dYs[] = {
			Board::DimensionY::Make(0, 0),
			Board::DimensionY::Make(0, tuning::ShipHeight-1)
		};
		Board::DimensionZ dZs[] = {
			Board::DimensionZ::Make(0, 0),
			Board::DimensionZ::Make(0, tuning::ShipDepth-1)
		};
		int const IX = index % 2;
		int const IY = (index/2)%2;
		int const IZ = (index/4)%2;
		origin.x += dXs[IX];
		origin.y += dYs[IY];
		origin.z += dZs[IZ];
		return origin;
	}
};

template <typename K, typename V>
struct StickyAssoc {
	K key;
	V value;
	bool valid = false;

	void TrySet(K newKey, V newValue) {
		if (key != newKey) {
			key = newKey;
			value = newValue;
			valid = true;
		}
	}
};

struct Game {
	Board board;
	int gravity;

	struct FrameState {
		Board::Coordinate shipPos;
		bool jumping;
		bool falling;
		FrameCount jumpFramesLeft;
		int speed;
		StickyAssoc<int, FrameNumber> turnStart;
	};

	void Reset(uint32_t seed) {
		gameLCG.Seed(seed);
		frameNumber = 0;
		gravity = -10;

		state = FrameState();
		Board::Coordinate shipPos = {};
#if 0
		shipPos.x = Board::DimensionX::Make(3, (tuning::TileWidth - tuning::ShipWidth) / 2);
		shipPos.y = Board::DimensionY::Make(1, 0);
		shipPos.z = Board::DimensionZ::Make(2, tuning::TileDepth / 8);
#else
		shipPos.x = Board::DimensionX::Make(3, (tuning::TileWidth - tuning::ShipWidth) / 2);
		shipPos.y = Board::DimensionY::Make(1, 0);
		shipPos.z = Board::DimensionZ::Make(0, 0);
#endif
		state.shipPos = shipPos;
		state.jumping = false;
		state.falling = false;
		state.jumpFramesLeft = 0;
		state.speed = 0;
	}

	LCG gameLCG;
	FrameState state;
	FrameNumber frameNumber;

	FrameState& State() { return state; }
	FrameState const& RenderState() const { return state; }

	void AdvanceFrame() {
		++frameNumber;
	}
};

struct HostContext {
	explicit HostContext(char const* title)
		: running(true)
	{
		SDL_Init(SDL_INIT_NOPARACHUTE | SDL_INIT_VIDEO | SDL_INIT_AUDIO);
		SDL_DisplayMode mode = {};
		SDL_GetCurrentDisplayMode(0, &mode);
		this->windowWidth = mode.w;
		this->windowHeight = mode.h;

		Mix_Init(0);
		Mix_OpenAudio(22050, AUDIO_S16LSB, 1, 1024);
		Mix_AllocateChannels(16);

		uint32_t windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS;
		this->window = SDL_CreateWindow("4-jump",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			windowWidth, windowHeight,
			windowFlags);
		bgfx::sdlSetWindow(window);
		bgfx::init();
#ifdef linux
		uint32_t const ResetFlags = BGFX_RESET_VSYNC;
#else
		uint32_t const ResetFlags = BGFX_RESET_MSAA_X16 | BGFX_RESET_VSYNC;
#endif
		uint32_t const DebugFlags = BGFX_DEBUG_TEXT;
		bgfx::reset(windowWidth, windowHeight, ResetFlags);
		bgfx::setDebug(DebugFlags);
		bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x101010FF);
	}

	~HostContext() {
		SDL_DestroyWindow(window);
		Mix_CloseAudio();
		while (Mix_Init(0)) {
			Mix_Quit();
		}
		SDL_Quit();
	}

	float WindowAspect() const { return (float)windowWidth / (float)windowHeight; }

	bool running;
	int windowWidth;
	int windowHeight;

	SDL_Window* window;
};

struct Input {
	bool wantAcceleration = false;
	bool wantBrake = false;
	bool wantLeft = false;
	bool wantRight = false;
	bool wantJump = false;
	FrameNumber wantJumpFrame = 0;

	static bool TestBinaryKey(SDL_Event const& event, SDL_Keycode key, bool* target = 0) {
		if (event.type != SDL_KEYDOWN && event.type != SDL_KEYUP)
			return false;
		if (event.key.keysym.sym != key)
			 return false;
		if (target) {
			*target = (event.type == SDL_KEYDOWN) ? true : false;
		}
		return true;
	}

	HostContext& host;
	float2 debugMousePos;
	int singleSteps;

	Input(HostContext& host)
		: host(host)
		, debugMousePos(0.5f, 0.5f)
		, singleSteps(0)
	{}

	void ConsumeEvent(SDL_Event& event) {
		if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE) {
			host.running = false;
		}
		if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_z) {
			singleSteps += 1;
		}
		if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_x) {
			singleSteps += 10;
		}
		if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_c) {
			singleSteps += 100;
		}
		if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_v) {
			singleSteps += 1000;
		}
		TestBinaryKey(event, SDLK_w, &wantAcceleration);
		TestBinaryKey(event, SDLK_s, &wantBrake);
		TestBinaryKey(event, SDLK_a, &wantLeft);
		TestBinaryKey(event, SDLK_d, &wantRight);
		if (event.type == SDL_MOUSEMOTION) {
			debugMousePos.x = event.motion.x / float(host.windowWidth-1);
			debugMousePos.y = event.motion.y / float(host.windowHeight-1);
		}
	}
};

int ZMain() {
	HostContext host("4-jump");

	Vertex7GM::Init();

	Game game;
	game.Reset(9001u);
	game.board = LoadDemoBoard();

	// TODO(zao): Load all the sounds and music
	//Mix_Chunk* sndIllegalMove = Mix_LoadWAV("data/sfx/illegal-move.wav");

	Mesh shipMesh = Load7GM("data/meshes/ship.7gm");
	TrackPieces::Init();

	DebugCubeSet debugCubes;

	bgfx::ProgramHandle singleProgram;
	bgfx::UniformHandle uLightCount;
	bgfx::UniformHandle uLightPs;
	bgfx::UniformHandle uLightCIs;
	{
		auto vsh = LoadShader("single_vs");
		auto fsh = LoadShader("single_fs");
		singleProgram = bgfx::createProgram(vsh, fsh, true);
		uLightCount = bgfx::createUniform("u_lightCount", bgfx::UniformType::Uniform1iv, 1);
		uLightPs = bgfx::createUniform("u_worldLight", bgfx::UniformType::Uniform4fv, 16);
		uLightCIs = bgfx::createUniform("u_lightColourIntensity", bgfx::UniformType::Uniform4fv, 16);
	}

	Frustum frustum;
	frustum.SetKind(FrustumSpaceD3D, FrustumRightHanded);
	frustum.SetViewPlaneDistances(0.1f, 5000.0f);

	int64_t const WallTickFrequency = bx::getHPFrequency();
	int64_t lastWallTicks = bx::getHPCounter();
	int64_t sdlTickBase = SDL_GetTicks();

	int64_t const TicksPerSimFrame = (int64_t)(WallTickFrequency / tuning::SimFramesPerSecond);
	int64_t simTickPool = 0;
	int64_t simFramesElapsed = 0;

	bool singleStep = false;

	Input input(host);

	int nearClip = -1;
	int farClip = +50;

	while (host.running) {
		debugCubes.Clear();
		SDL_Event event = {};
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				host.running = false;
			}
			else {
				input.ConsumeEvent(event);
			}
		}
		if (!host.running) {
			break;
		}

		// NOTE(zao): Simulating
		int64_t wallTicks = bx::getHPCounter();
		int64_t newTicks = wallTicks - lastWallTicks;
		int64_t const MaxTicksToSim = (int64_t)(0.25f * WallTickFrequency);
		if (newTicks > MaxTicksToSim)
			newTicks = MaxTicksToSim;
		lastWallTicks = wallTicks;

		simTickPool += newTicks;
		while (simTickPool >= TicksPerSimFrame) {
			auto& state = game.State();

			if (!singleStep || (singleStep && input.singleSteps)) {
				// Gather steering input
				bool sideGrinding = false;
				int xDelta = 0;
				if (input.wantLeft != input.wantRight) {
					int dir = (input.wantLeft ? -1 : 1);
					int speed = dir * tuning::SideSpeedPerFrame;
					xDelta = speed;
					state.turnStart.TrySet(dir, game.frameNumber);
				}
				else {
					state.turnStart.TrySet(0, game.frameNumber);
				}

				struct Cell {
					int x, y, z;

					static Cell Make(Board::Coordinate coord) {
						Cell ret;
						ret.x = coord.x.Cell();
						ret.y = coord.y.Cell();
						ret.z = coord.z.Cell();
						return ret;
					}

					bool operator == (Cell o) const {
						return x == o.x && y == o.y && z == o.z;
					};

					bool operator != (Cell o) const {
						return x != o.x || y != o.y || z != o.z;
					}

					bool operator < (Cell o) const {
						if (x != o.x) return x < o.x;
						if (y != o.y) return y < o.y;
						return z < o.z;
					}
				};

				struct WorldRect {
					Board::Coordinate low;
					Board::Coordinate high;
				};

				struct WorldElement {
					WorldElement(Board& board, Cell cell)
						: board(board)
						, x(cell.x), y(cell.y), z(cell.z)
					{
					}

					int BlockCount() const {
						if (!board.IsValidCell(x, y, z)) return 0;
						auto elem = board.GetElementRefByCell(x, y, z);
						if (!elem.IsPresent()) return 0;
						if (elem.IsTunnel()) {
							return 3;
						}
						return 1;
					}

					struct Block {
						Board::Coordinate low;
						Board::Coordinate high;
					};

					static bool TestBlockBlock(Block a, Block b) {
						auto aMin = (int const*)(&a.low);
						auto aMax = (int const*)(&a.high);
						auto bMin = (int const*)(&b.low);
						auto bMax = (int const*)(&b.high);

						if (aMax[0] < bMin[0] || aMin[0] > bMax[0]) return 0;
						if (aMax[1] < bMin[1] || aMin[1] > bMax[1]) return 0;
						if (aMax[2] < bMin[2] || aMin[2] > bMax[2]) return 0;
						return 1;
					}

					static bool IntersectMovingBlockBlock(Block a, Block b, Board::Coordinate vA, Board::Coordinate vB, float& tFirst, float& tLast) {
						if (TestBlockBlock(a, b)) {
							tFirst = tLast = 0.0f;
							return true;
						}

						float3 v = {
							(float)(vB.x - vA.x).value,
							(float)(vB.y - vA.y).value,
							(float)(vB.z - vA.z).value };
						tFirst = 0.0f;
						tLast = 1.0f;

						auto aMin = (int const*)(&a.low);
						auto aMax = (int const*)(&a.high);
						auto bMin = (int const*)(&b.low);
						auto bMax = (int const*)(&b.high);

						for (int i = 0; i < 3; ++i) {
							if (v[i] < 0.0f) {
								if (bMax[i] < aMin[i]) return 0;
								if (aMax[i] < bMin[i]) tFirst = Max((aMax[i] - bMin[i]) / v[i], tFirst);
								if (bMax[i] > aMin[i]) tLast  = Min((aMin[i] - bMax[i]) / v[i], tLast);
							}
							if (v[i] > 0.0f) {
								if (bMin[i] > aMax[i]) return 0;
								if (bMax[i] < aMin[i]) tFirst = Max((aMin[i] - bMax[i]) / v[i], tFirst);
								if (aMax[i] > bMin[i]) tLast  = Min((aMax[i] - bMin[i]) / v[i], tLast);
							}
							if (tFirst > tLast) return 0;
						}
						return 1;
					}

					Block BlockDefinition(int index) const {
						assert(index >= 0 && index < BlockCount());
						auto elem = board.GetElementRefByCell(x, y, z);
						assert(elem.IsPresent());
						Board::DimensionX xs[] = {
							Board::DimensionX::Make(x, 0),
							Board::DimensionX::Make(x, tuning::TunnelWallThickness),
							Board::DimensionX::Make(x+1, -tuning::TunnelWallThickness),
							Board::DimensionX::Make(x+1, 0) };
						Board::DimensionY ys[] = {
							Board::DimensionY::Make(y, 0),
							Board::DimensionY::Make(y+1, -tuning::TunnelCeilingThickness),
							Board::DimensionY::Make(y+1, 0) };
						Board::DimensionZ zs[] = {
							Board::DimensionZ::Make(z, 0),
							Board::DimensionZ::Make(z+1, 0) };
						Block ret = {
							{ xs[0], ys[0], zs[0] },
							{ xs[3], ys[2], zs[1] } };
						if (!elem.IsTunnel()) {
							return ret;
						}

						switch (index) {
							case 0: { // Left wall
								ret.high.x = xs[1];
							} break;
							case 1: { // Right wall
								ret.low.x = xs[2];
							} break;
							case 2: { // Ceiling
								ret.low.x = xs[1];
								ret.high.x = xs[2];
								ret.low.y = ys[1];
								ret.high.y = ys[2];
							} break;
						}
						return ret;
					}

					Board& board;
					int x, y, z;
				};

				auto const Sign = [](int x) -> int { if (x > 0) return 1; if (x < 0) return -1; return 0; };

				bool sideCollide = false;
				// First sideway movement
				if (xDelta) {
					int const Axis = 0;
					int dir = Sign(xDelta);
					int mag = abs(xDelta);
					auto base = state.shipPos;
					auto corner = base;
					corner.x.value += tuning::ShipWidth;
					corner.y.value += tuning::ShipHeight;
					corner.z.value += tuning::ShipDepth;
					WorldElement::Block shipBlock;
					shipBlock.low = base;
					shipBlock.high = corner;

					auto baseCell = Cell::Make(base);
					for (int xOffset = -1; xOffset <= 1; ++xOffset) {
						for (int yOffset = -1; yOffset <= 1; ++yOffset) {
							for (int zOffset = -1; zOffset <= 1; ++zOffset) {
								Cell cell = baseCell;
								cell.x += xOffset;
								cell.y += yOffset;
								cell.z += zOffset;
								WorldElement elem(game.board, cell);
								for (int iBlock = 0; iBlock < elem.BlockCount(); ++iBlock) {
									WorldElement::Block wallBlock = elem.BlockDefinition(iBlock);
									Board::Coordinate shipVelocity = { { xDelta }, { 0 }, { 0 } };
									Board::Coordinate wallVelocity = { };
									float tFirst, tLast;
									if (elem.IntersectMovingBlockBlock(shipBlock, wallBlock,
												shipVelocity, wallVelocity,
												tFirst, tLast))
									{
										xDelta = (int)(xDelta * tFirst);
										sideCollide = true;
									}
								}
							}
						}
					}
#if 0
					int Dir = xDelta < 0 ? -1 : 1;
					for (int oY = 0; oY < 2; ++oY) {
						auto sY = y + Board::DimensionY::Make(0, oY * (tuning::ShipHeight - 1));
						for (int oZ = 0; oZ < 2; ++oZ) {
							auto sZ = z + Board::DimensionZ::Make(0, oZ * (tuning::ShipDepth - 1));
							auto* e = game.board.GetElementPtrByCell(cellX+Dir, sY.Cell(), sZ.Cell());
							if (e) {
								if (e->IsPresent()) {
									int dX = -x.Frac();
									if (Dir > 0) {
										dX += Board::DimensionX::Granularity - tuning::ShipWidth;
									}
									if (abs(xDelta) > abs(dX)) {
										xDelta = dX;
									}
								}
							}
						}
					}
#endif
					state.shipPos.x.value += xDelta;
				}

				// Gather speed changes
				if (input.wantAcceleration != input.wantBrake) {
					int dir = (input.wantAcceleration ? 1 : -1);
					int dSpeed = dir * tuning::SpeedChangePerFrame;
					state.speed = Min(tuning::MaxSpeed, Max(tuning::MinSpeed, state.speed + dSpeed));
				}
				// TODO(zao): Slow down on grinding and corner adjustments.

				// TODO(zao): Detect/handle forward collisions.
				int zDelta = state.speed / tuning::SimFramesPerSecond;
				if (zDelta) {
#if 1
					auto x = state.shipPos.x;
					auto y = state.shipPos.y;
					auto z = state.shipPos.z;
					Board::DimensionX xL = x;
					Board::DimensionX xM = x + Board::DimensionX::Make(0, tuning::ShipWidth / 2);
					Board::DimensionX xH = x + Board::DimensionX::Make(0, tuning::ShipWidth - 1);
					bool differentCellsX = xL.Cell() != xH.Cell();
					int cellX = state.shipPos.x.Cell();
					int cellY = state.shipPos.y.Cell();
					int cellZ = state.shipPos.z.Cell();
					for (int oY = 0; oY < 2; ++oY) {
						auto sY = y + Board::DimensionY::Make(0, oY * (tuning::ShipHeight - 1));
						auto* eL = game.board.GetElementPtrByCell(xL.Cell(), sY.Cell(), cellZ);
						auto* eH = game.board.GetElementPtrByCell(xH.Cell(), sY.Cell(), cellZ);
						if (eL == eH) {
							if (!eL->IsPresent()) {
								// Good
							}
							else if (eL->shape == Board::SolidShape) {
								// Fatal
								abort();
							}
							else {
								assert(eL->shape == Board::FreeTunnelShape || eL->shape == Board::BlockTunnelShape);
								auto const TW = Board::DimensionX::Granularity;
								auto const killW = tuning::TileWidth / 32;
								auto const nudgeW = tuning::TileWidth / 16;
								if (xL.Frac() < killW || xH.Frac() >= TW - killW) {
									// Fatal
									abort();
								}
								else if (xL.Frac() < nudgeW || xH.Frac() >= TW - nudgeW) {
									// Nudge
								}
								else {
									// Good
								}
							}
						}
					}
#else
					ShipContacts contacts;
					for (int iSegment = 0; iSegment < contacts.GetSegmentCount(); ++iSegment) {
						auto segment = contacts.GetSegment(iSegment, state.shipPos);
						Board::DimensionZ collideZ;
						if (game.board.ForwardSegment(segment, &collideZ)) {
							// TODO(zao): Destroy ship or nudge if near-miss.
						}
					}
#endif
				}

				// Update depth movement
				state.shipPos.z.value += zDelta;

				// And finally vertical movement
				{
					ShipContacts contacts;
					Maximizer<Board::DimensionY> highestY(Board::DimensionY::Make(-20, 0));
					for (int iContact = 0; iContact < contacts.GetPointCount(); ++iContact) {
						Board::DimensionY groundY;
						if (game.board.DownRay(contacts.GetPoint(iContact, state.shipPos), &groundY)) {
							++groundY.value;
							highestY.TrySet(groundY);
						}
					}
					Board::DimensionY speed = Board::DimensionY::Make(0, game.gravity);
					Board::DimensionY dY = highestY.value - state.shipPos.y;
					speed = Max(speed, dY);
					//state.shipPos.y += speed;
				}

				++simFramesElapsed;
				if (input.singleSteps) {
					--input.singleSteps;
				}
			}
			simTickPool -= TicksPerSimFrame;
			game.AdvanceFrame();
		}

		bgfx::dbgTextClear();
		bgfx::dbgTextPrintf(0, 9, 0x1F, "%s", game.state.shipPos.ToString().c_str());
		bgfx::dbgTextPrintf(0, 11, 0x5F, "Clip n/f: %d %d", nearClip, farClip);

		LCG debugLCG(9001);
		{
			ShipContacts contacts;
			for (int i = 0; i < contacts.GetPointCount(); ++i) {
				Board::Coordinate base = game.state.shipPos;
				debugCubes.Add(contacts.GetPoint(i, base), float4::one);
			}
		}
		for (int i = 0; i < Board::ElementCount; ++i) {
			int x = i;
			float4 color = float4(float3::RandomBox(debugLCG, 0.0f, 1.0f), 1.0f);
			Board::Coordinate p = {
				Board::DimensionX::Make(i, 0),
				Board::DimensionY::Make(0, 0),
				Board::DimensionZ::Make(0, 0)
			};
			debugCubes.Add(p, color);
		}

		// NOTE(zao): Drawing
		auto& renderState = game.RenderState();
		float const T = game.frameNumber / (float)tuning::SimFramesPerSecond;

		float3 shipWorldPos;
		{
			Board::Coordinate shipLeft = renderState.shipPos;
			Board::Coordinate shipMid = shipLeft;
			shipMid.x += Board::DimensionX::Make(0, tuning::ShipWidth / 2);
			shipMid.z += Board::DimensionZ::Make(0, tuning::ShipDepth / 2);
			shipWorldPos = ResolveCoordinate(shipMid);
		}

		float vFov = Lerp(pi/8.0f, pi/2.0f, input.debugMousePos.x);
		vFov = pi/3.0f;
		bgfx::dbgTextPrintf(0, 3, 0x3F, "Ship: %s", game.state.shipPos.ToString().c_str());
		frustum.SetVerticalFovAndAspectRatio(vFov, host.WindowAspect());
		{
			float3 base = float3::zero;
			base.x = tuning::TileWidth * 3.5f * tuning::TileWorldScale;
			base.y = 0.0f;
			base.z = shipWorldPos.z;
			float3 eye = base + float3(0.0f, 25.0f, 50.0f);
			float3 at = base + float3(0.0f, 15.0f, -150.0f);
			float3x4 W = float3x4::LookAt(eye, at, -float3::unitZ, float3::unitY, float3(0.0f, 1.0f, -1.0f).Normalized());
			frustum.SetWorldMatrix(W);
		}
		bgfx::setViewRect(0, 0, 0, host.windowWidth, host.windowHeight);
		{
			float4x4 const ViewMatrix = frustum.ViewMatrix();
			float4x4 const ProjectionMatrix = frustum.ProjectionMatrix();
			bgfx::setViewTransform(0, ViewMatrix.Transposed().ptr(), ProjectionMatrix.Transposed().ptr());
		}
		bgfx::submit(0);

		int lightCount = 2;
		float4 lightPositions[16] = {
			float4(0.0f, 200.0f, 50.0f + shipWorldPos.z, 0.0f),
			float4(shipWorldPos + float3(0.0f, 1.0f, 0.0f), 1.0f),
		};
		float4 lightAttributes[16] = {
			float4(float3(1.0f, 1.0f, 1.0f), 5000.0f),
			float4(float3(-0.2f, -0.2f, -0.2f), 10.0f),
		};

		// TODO(zao): Move this out to draw a mesh
		auto const& DrawThing = [&](Mesh* m, float3 offset, Quat R) {
			bgfx::setProgram(singleProgram);
			bgfx::setUniform(uLightCount, &lightCount, 1);
			bgfx::setUniform(uLightPs, lightPositions, 16);
			bgfx::setUniform(uLightCIs, lightAttributes, 16);
			float3x4 T = float3x4::Translate(offset);
			float4x4 M = T * R;
			bgfx::setVertexBuffer(m->vbh);
			bgfx::setTransform(M.Transposed().ptr());
			bgfx::setState(BGFX_STATE_DEFAULT);
			bgfx::submit(0);
		};

		auto const& DrawShip = [&](float3 offset, Quat R) {
			bgfx::setProgram(singleProgram);
			int lightCount = 1;
			bgfx::setUniform(uLightCount, &lightCount, 1);
			bgfx::setUniform(uLightPs, lightPositions, 16);
			bgfx::setUniform(uLightCIs, lightAttributes, 16);
			float3x4 T = float3x4::Translate(offset);
			float4x4 M = T * R;
			bgfx::setVertexBuffer(shipMesh.vbh);
			bgfx::setTransform(M.Transposed().ptr());
			bgfx::setState(BGFX_STATE_DEFAULT);
			bgfx::submit(0);
		};

		int shipZ = renderState.shipPos.z.Cell();
		for (size_t iZ = 0; iZ < game.board.slices.size(); ++iZ) {
			int relZ = (int)iZ - shipZ;
			if (relZ < nearClip) continue;
			if (relZ > farClip) break;
			auto const& slice = game.board.slices.at(iZ);
			for (size_t iT = 0; iT < Board::TierCount; ++iT) {
				auto const& tier = slice.tiers[iT];
				for (size_t iE = 0; iE < Board::ElementCount; ++iE) {
					auto const& elem = tier.elements[iE];
					if (elem.IsPresent()) {
						Board::Coordinate coord = {
							Board::DimensionX::Make((int)iE, 0),
							Board::DimensionY::Make((int)iT, 0),
							Board::DimensionZ::Make((int)iZ, 0)
						};
						auto pos = ResolveCoordinate(coord);
						Mesh m = {};
						switch (elem.shape) {
							case Board::SolidShape:
								m = TrackPieces::GetMesh(iT ? TrackPieces::Piece::Block : TrackPieces::Piece::GroundSlab);
								break;
							case Board::BlockTunnelShape:
								m = TrackPieces::GetMesh(TrackPieces::Piece::BlockTunnel);
								break;
							case Board::FreeTunnelShape:
								m = TrackPieces::GetMesh(TrackPieces::Piece::FreeTunnel);
								break;
							case Board::NoShape:
								;
						}
						if (m.vertexCount) {
							DrawThing(&m, pos, Quat::identity);
						}
					}
				}
			}
		}
		{
			static LCG shipLCG = debugLCG;
			Quat jiggle = Quat::identity; //Quat::RandomRotation(shipLCG).Lerp(Quat::identity, 0.99f);
			Quat LR = Quat::identity;
			if (game.state.turnStart.valid && game.state.turnStart.key) {
				auto turnSide = game.state.turnStart.key;
				auto startFrame = game.state.turnStart.value;
				int const FrameDiff = (int)(game.frameNumber - startFrame);
				float const AmountY = Lerp(0.0f, 0.1f,
						Clamp01(FrameDiff / 5.0f));
				float const AmountZ = Lerp(0.0f, 0.3f,
						Clamp01(FrameDiff / 5.0f));
				float const Dir = (turnSide > 0 ? -1.0f : 1.0f);
				LR = Quat::RotateY(AmountY * Dir) *
					Quat::RotateZ(AmountZ * Dir);
			}
			// TODO(zao): Nudge temporary until mesh is adjusted
			float3 pos = shipWorldPos + float3(0.0f, 1.0f, 0.0f);
			DrawShip(pos, jiggle * LR);
		}
		debugCubes.Draw();

		bgfx::frame();
	}
	return 0;
}

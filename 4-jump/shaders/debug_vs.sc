$input a_position, a_texcoord, a_normal
$output v_texcoord, v_worldNormal, v_worldPos

#include <bgfx_shader.sh>

void main()
{
	vec4 pos = vec4(a_position, 1.0);
	gl_Position = mul(u_modelViewProj, pos);
	v_texcoord = a_texcoord;
	mat4 normalMatrix = u_model[0];
	v_worldNormal = mul(normalMatrix, vec4(a_normal, 0.0)).xyz;
	v_worldPos = mul(u_model[0], pos).xyz;
}
$input v_texcoord, v_worldNormal, v_worldPos

#include <bgfx_shader.sh>

uniform vec4 uBaseColor;

void main()
{
	vec3 N = normalize(v_worldNormal);
	vec3 aboveColor = vec3(0.7f, 0.7f, 0.9f);
	vec3 belowColor = vec3(0.7f, 0.7f, 0.6f);
	float w = 0.5f * (1.0f + dot(vec3(0.0f, 1.0f, 0.0f), N));
	vec3 hemiLight = mix(belowColor, aboveColor, w);
	gl_FragColor = vec4(uBaseColor.rgb * hemiLight, 1.0f);
	gl_FragColor = uBaseColor;
}

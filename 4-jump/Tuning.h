#pragma once

#include <MathGeoLib/MathGeoLib.h>
#include <stdint.h>

typedef int64_t FrameCount;
typedef int64_t FrameNumber;

namespace tuning {
	FrameCount const SimFramesPerSecond = 60;
	int const ViewWidth = 1280;
	int const ViewHeight = 720;
	float const ViewAspect = ViewWidth / ViewHeight;

	int const TileWidth = 256;
	int const TileHeight = TileWidth / 2;
	int const TileDepth = TileWidth * 3;

	int const TunnelWallThickness = TileWidth / 32;
	int const TunnelCeilingThickness = TileHeight / 16;

	float const TileWorldScale = 10.0f / 256.0f;

	int const ShipWidth = 192;
	int const ShipHeight = 64;
	int const ShipDepth = 128;

	int const SideSpeedPerFrame = 10;
	int const MinSpeed = 0;
	int const MaxSpeed = 2560;
	int const SpeedChangePerFrame = 20;
}

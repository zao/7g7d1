#!/usr/bin/env bash
if [ "$(uname)" == "Darwin" ]
then
	SHADERC=${HOME}/bounce/bgfx/.build/osx64_clang/bin/shadercRelease
else
	SHADERC=${HOME}/bounce/bgfx/.build/linux64_gcc/bin/shadercRelease
fi

function compile_linux_glsl () {
	$SHADERC -I shaders --platform linux --varyingdef shaders/single_def.sc \
	--profile 120 --type $1 -f shaders/$2.sc -o data/shaders/glsl/$2.bin
}

mkdir -p data/shaders/dx11
mkdir -p data/shaders/glsl
compile_linux_glsl vertex debug_vs
compile_linux_glsl frag   debug_fs
compile_linux_glsl vertex single_vs
compile_linux_glsl frag   single_fs

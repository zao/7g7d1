#include "OS.h"
#include <fstream>
#include <iostream>
#include <vector>

#ifdef __APPLE__
#include <CoreFoundation/CoreFoundation.h>
std::string GetApplicationRoot() {
	std::ofstream os("/tmp/debug.txt");
	CFBundleRef bundle = CFBundleGetMainBundle();
	CFURLRef url = CFBundleCopyExecutableURL(bundle);
	std::string ret;
	std::vector<uint8_t> path(1024*1024);
	if (CFURLGetFileSystemRepresentation(url, true, path.data(), path.size())) {
		ret = std::string((char const*)path.data());
		os << ret << std::endl;
		ret = ret.substr(0, ret.find_last_of('/'));
		ret = ret.substr(0, ret.find_last_of('/'));
		os << ret << std::endl;
	}
	return ret;
}
#else
std::string GetApplicationRoot() {
	return "";
}
#endif

#!/usr/bin/env bash
mkdir -p bin
CXXFLAGS="-std=gnu++11 -g"

if [ "$(uname)" == "FreeBSD" ]
then
	CXX=clang++
else
	CXX=g++
fi

CPPFLAGS="\
	-I$HOME/bounce/bx/include \
	-I$HOME/bounce/bgfx/include \
	-I$HOME/bounce/MathGeoLib/include \
	-I$HOME/opt/MathGeoLib-git/include \
	-I$HOME/lap/MathGeoLib/git/include \
	-I$HOME/opt/SDL2_mixer-2.0.0/include \
	-I$HOME/opt/SDL2_mixer-2.0.0/include/SDL2 \
	-I/usr/include/SDL2 \
	-I/usr/local/include \
	-D__STDC_LIMIT_MACROS=1 \
	-D__STDC_FORMAT_MACROS=1 \
	-D__STDC_CONSTANT_MACROS=1 \
"

LDFLAGS="\
	-L$HOME/bounce/bgfx/.build/linux64_gcc/bin \
	-L$HOME/bounce/bgfx/.build/osx64_clang/bin \
	-L$HOME/bounce/MathGeoLib/build \
	-L$HOME/opt/MathGeoLib-git/lib \
	-L$HOME/lap/MathGeoLib/git/lib \
	-L$HOME/opt/SDL2_mixer-2.0.0/lib \
	-L/usr/local/lib \
	-Wl,-rpath -Wl,$HOME/opt/SDL2_mixer-2.0.0/lib \
"

LIBS="\
-lMathGeoLib \
"

FRAMEWORKS=""

if [ "$(uname)" == "Darwin" ]
then
	FRAMEWORKS=" \
	-framework SDL2 \
	-framework SDL2_mixer \
	-framework OpenGL \
	-framework Foundation \
	-framework Cocoa \
	"
else
	if [ "$(uname)" == "FreeBSD" ]
	then
		if [ ! -f bin/bgfx.o ]; then
			clang++ \
				$CPPFLAGS $CXXFLAGS $LDFLAGS \
				-o bin/bgfx.o \
				-c $HOME/bounce/bgfx/src/amalgamated.cpp \
				-I $HOME/bounce/bgfx/include \
				-I $HOME/bounce/bx/include \
				-I $HOME/bounce/bx/include/compat/freebsd \
				-I $HOME/bounce/bgfx/3rdparty/khronos
		fi
		LIBS="bin/bgfx.o \
		$LIBS \
		"
	else
		LIBS="$LIBS \
		-lbgfxRelease \
		-ldl \
		"
	fi
	LIBS="$LIBS \
	-pthread \
	-lSDL2 -lX11 -lGL \
	-lSDL2_mixer \
	"
fi

# $CXX $CXXFLAGS $CPPFLAGS \
# 	-x c++ \
# 	-o /dev/null \
# 	-c Utils.h
# exit

$CXX $CXXFLAGS $CPPFLAGS $LDFLAGS \
	-o bin/4-jump \
	Board.cc Formats.cc JumpMain.cc OS.cc TrackPieces.cc Utils.cc \
	$LIBS $FRAMEWORKS

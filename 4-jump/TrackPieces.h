#pragma once

#include "Formats.h"

namespace TrackPieces {
	namespace Piece {
		enum Type {
			GroundSlab,
			Block,
			FreeTunnel,
			BlockTunnel,
			Count
		};
	}
	void Init();
	Mesh GetMesh(Piece::Type type);
}

#include "Board.h"

#include <algorithm>

template <typename T>
T Lowest(T a, T b) {
	return (b < a) ? b : a;
}

Board LoadBoard(std::vector<std::string> const& lines) {
	assert(lines.size() != 0);
	assert(lines.size() % 7 == 0);
	Board ret;
	int const TierCount = Lowest((int)(lines.size() / 7), Board::TierCount);
	int const SliceCount = (int)std::max_element(lines.begin(), lines.end(),
		[](std::string const& a, std::string const& b) { return a.size() < b.size(); })->size();
	Board::Slice slice;
	memset(&slice, 0, sizeof(slice));
	ret.slices.resize(SliceCount, slice);
	std::string const* line = &lines.at(0);
	for (int iT = 0; iT < TierCount; ++iT) {
		bool const GroundLevel = (iT == 0);
		for (int iE = 0; iE < 7; ++iE) {
			for (size_t iS = 0; iS < line->size(); ++iS) {
				auto* elem = &ret.slices.at(iS).tiers[iT].elements[iE];
				char ch = line->at(iS);
				if (ch == ' ') continue;
				elem->effect = Board::NoEffect;
				if (GroundLevel) switch (ch) {
					// TODO(zao): Damage/boost/brake/slip/fuel/oxygen
					case '#':
					case 'X':
					case '>':
					case '<':
					case 'F':
					case 'O':
					case 'S':
						elem->shape = Board::SolidShape;
						break;
					default:
						abort();
				}
				else switch (ch) {
					case '#': {
						elem->shape = Board::SolidShape;
					} break;
					case '-': {
						elem->shape = Board::BlockTunnelShape;
					} break;
					case '=': {
						elem->shape = Board::FreeTunnelShape;
					} break;
					default:
						abort();
				}
			}
			++line;
		}
	}
	return ret;
}

struct FromToInclusive {
	FromToInclusive(int from, int to)
		: dir(from < to ? 1 : -1)
	{}

	int from, to;
	int dir;

	void Next() {
		from += dir;
	}

	bool Done() const {
		return from - dir == to;
	}
};

struct FromToExclusive {
	FromToExclusive(int from, int to)
		: dir(from < to ? 1 : -1)
	{}

	int from, to;
	int dir;

	void Next() {
		from += dir;
	}

	bool Done() const {
		return from == to;
	}
};

bool Board::IsValidCell(int cellX, int cellY, int cellZ) const {
	return
		cellX >= 0 && cellX < Board::ElementCount &&
		cellY >= 0 && cellY < Board::TierCount &&
		cellZ >= 0 && cellZ < slices.size();
}

Board::Element& Board::GetElementRefByCell(int cellX, int cellY, int cellZ) {
	if (!IsValidCell(cellX, cellY, cellZ)) abort();
	return slices.at(cellZ).tiers[cellY].elements[cellX];
}

Board::Element const& Board::GetElementRefByCell(int cellX, int cellY, int cellZ) const {
	if (!IsValidCell(cellX, cellY, cellZ)) abort();
	return slices.at(cellZ).tiers[cellY].elements[cellX];
}

Board::Element* Board::GetElementPtrByCell(int cellX, int cellY, int cellZ) {
	if (!IsValidCell(cellX, cellY, cellZ)) return nullptr;
	return &slices.at(cellZ).tiers[cellY].elements[cellX];
}

Board::Element const* Board::GetElementPtrByCell(int cellX, int cellY, int cellZ) const {
	if (!IsValidCell(cellX, cellY, cellZ)) return nullptr;
	return &slices.at(cellZ).tiers[cellY].elements[cellX];
}

bool Board::SideRay(Coordinate pos, int dir, DimensionX* outX) const {
	int cellX = pos.x.Cell();
	int cellY = pos.y.Cell();
	int cellZ = pos.z.Cell();
	if (cellY < 0 || cellY >= Board::TierCount) return false;
	if (cellZ < 0 || cellZ >= slices.size()) return false;

	if (dir > 0) {
		for (int X = cellX+1; X < Board::ElementCount; ++X) {
			auto& elem = GetElementRefByCell(X, cellY, cellZ);
			if (elem.IsPresent()) {
				if (outX) *outX = DimensionX::Make(X, 0);
				return true;
			}
		}
	}
	else {
		for (int X = cellX-1; X >= 0; --X) {
			auto& elem = GetElementRefByCell(X, cellY, cellZ);
			if (elem.IsPresent()) {
				if (outX) *outX = DimensionX::Make(X+1, -1);
				return true;
			}
		}
	}

	return false;
}

bool Board::DownRay(Coordinate pos, DimensionY* outY) const {
	int cellX = pos.x.Cell();
	int cellY = pos.y.Cell();
	int cellZ = pos.z.Cell();
	if (cellX < 0 || cellX >= 7) return false;
	if (cellZ < 0 || cellZ >= slices.size()) return false;

	for (int Y = cellY; Y >= 0; --Y) {
		if (Board::TierCount <= Y) continue;
		auto& elem = GetElementRefByCell(cellX, Y, cellZ);
		if (elem.IsPresent()) {
			if (outY) *outY = DimensionY::Make(Y+1, -1);
			return true;
		}
	}
	return false;
}

bool Board::ForwardSegment(LineSegment seg, DimensionZ* outZ) const {
	return false;
}

Board LoadDemoBoard() {
	std::vector<std::string> lines = {
		"#    ###   " + std::string(3000, '#'),
		"           " + std::string(3000, '#'),
		"###  ##    " + std::string(3000, '#'),
		"###########" + std::string(3000, '#'),
		"###  ##    " + std::string(3000, '#'),
		"#          " + std::string(3000, '#'),
		"###########" + std::string(3000, '#'),
		"",
		"##",
		"#    ##",
		"  ===--=",
		"#    ##",
		"#",
		"",
	};
	Board ret = LoadBoard(lines);
	return ret;
}


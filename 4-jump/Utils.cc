#include "Utils.h"

void DebugCubeSet::Clear() {
	cubes.clear();
}

void DebugCubeSet::Add(Board::Coordinate pos, uint32_t tint) {
	float4 color = {
		((tint >> 24) & 0xFF) / 255.0f,
		((tint >> 16) & 0xFF) / 255.0f,
		((tint >> 8)  & 0xFF) / 255.0f,
		((tint >> 0)  & 0xFF) / 255.0f
	};
	Add(pos, color);
}

void DebugCubeSet::Add(Board::Coordinate pos, float4 tint) {
	Cube c = { pos, tint };
	cubes.push_back(c);
}

void DebugCubeSet::Draw() {
	for (auto& cube : cubes) {
		float3 offset = ResolveCoordinate(cube.pos);
		bgfx::setProgram(program);
		bgfx::setUniform(uBaseColor, &cube.color, 1);
		float3x3 S = float3x3::UniformScale(10 * tuning::TileWorldScale);
		float3x4 T = float3x4::Translate(offset);
		float4x4 M = T * S;
		bgfx::setVertexBuffer(mesh.vbh);
		bgfx::setTransform(M.Transposed().ptr());
		bgfx::setState(BGFX_STATE_DEFAULT);
		bgfx::submit(0);
	}
}

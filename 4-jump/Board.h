#pragma once
#include "TrackPieces.h"
#include "Tuning.h"
#include <array>
#include <vector>

struct Board {
	enum PieceShape {
		NoShape,
		SolidShape,
		BlockTunnelShape,
		FreeTunnelShape,
	};
	enum PieceEffect {
		NoEffect,
		BoostEffect,
		BrakeEffect,
		BurnEffect,
		RestockEffect,
		SlideEffect,
	};
	static int const TierCount = 4;
	static int const ElementCount = 7;
	struct Element {
		bool IsPresent() const { return shape != NoShape; }
		bool IsTunnel() const { return shape == BlockTunnelShape || shape == FreeTunnelShape; }
		PieceShape shape;
		PieceEffect effect;
	};
	struct Tier {
		Element elements[7];
	};
	struct Slice {
		Tier tiers[4];
	};
	std::vector<Slice> slices;

	template <size_t CellSize>
	struct Dimension
	{
		static int const Granularity = CellSize;
		int value;

		static Dimension Make(int cells, int fracs) {
			Dimension ret;
			ret.value = cells * CellSize + fracs;
			return ret;
		}

		int Cell() const {
			return value / CellSize;
		}

		int Frac() const {
			return value % CellSize;
		}

		std::string ToString() const {
			return std::to_string(Cell()) + ":{" + std::to_string(Frac()) + "/" + std::to_string(CellSize) + "}";
		}

#define DEFINE_DIMENSION_RELATIONAL_OPERATION(Op) bool operator Op (Dimension rhs) const { return value Op rhs.value; }
		DEFINE_DIMENSION_RELATIONAL_OPERATION(<)
		DEFINE_DIMENSION_RELATIONAL_OPERATION(>)
		DEFINE_DIMENSION_RELATIONAL_OPERATION(==)
		DEFINE_DIMENSION_RELATIONAL_OPERATION(<=)
		DEFINE_DIMENSION_RELATIONAL_OPERATION(>=)
		DEFINE_DIMENSION_RELATIONAL_OPERATION(!=)
#undef DEFINE_DIMENSION_RELATIONAL_OPERATION

#define DEFINE_DIMENSION_BINARY_OPERATION(Op) \
		Dimension& operator Op##= (Dimension rhs) { \
			value Op##= rhs.value; \
			return *this; \
		} \
		Dimension operator Op (Dimension rhs) const { \
			Dimension ret = *this; \
			ret Op##= rhs; \
			return ret; \
		}
		DEFINE_DIMENSION_BINARY_OPERATION(+)
		DEFINE_DIMENSION_BINARY_OPERATION(-)
#undef DEFINE_DIMENSION_BINARY_OPERATION
	};

	typedef Dimension<tuning::TileWidth> DimensionX;
	typedef Dimension<tuning::TileHeight> DimensionY;
	typedef Dimension<tuning::TileDepth> DimensionZ;

	struct Coordinate {
		DimensionX x;
		DimensionY y;
		DimensionZ z;

		std::string ToString() const {
			std::ostringstream oss;
			oss << "(" << x.ToString() << " | " << y.ToString() << " | " << z.ToString() << ")";
			return oss.str();
		}
	};

	typedef std::array<Board::Coordinate, 2> LineSegment;
	
	bool IsValidCell(int cellX, int cellY, int cellZ) const;
	Element& GetElementRefByCell(int cellX, int cellY, int cellZ);
	Element const& GetElementRefByCell(int cellX, int cellY, int cellZ) const;
	Element* GetElementPtrByCell(int cellX, int cellY, int cellZ);
	Element const* GetElementPtrByCell(int cellX, int cellY, int cellZ) const;

	bool SideRay(Coordinate pos, int dir, DimensionX* outX = nullptr) const;
	bool DownRay(Coordinate pos, DimensionY* outY = nullptr) const;
	bool DownQuery(Coordinate pos, Coordinate* outCoord = nullptr) const;
	bool ForwardSegment(LineSegment seg, DimensionZ* outZ = nullptr) const;
};

Board LoadBoard(std::vector<std::string> const& lines);
Board LoadDemoBoard();

inline auto ResolveCoordinate(Board::Coordinate coord) -> float3 {
	float3 ret = float3::zero;
	ret.x += +coord.x.value * tuning::TileWorldScale;
	ret.y += +coord.y.value * tuning::TileWorldScale;
	ret.z += -coord.z.value * tuning::TileWorldScale;
	return ret;
};


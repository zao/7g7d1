#pragma once

#include <bgfx.h>
#include <deque>
#include <functional>
#include "Board.h"

template <typename T, typename Aspect>
struct Optimizer {
	bool set = false;
	T value;

	Optimizer()
	{}

	Optimizer(T initial)
		: set(true)
		, value(initial)
	{}

	bool TrySet(T candidate) {
		if (!set) {
			set = true;
			value = candidate;
			return true;
		}
		Aspect op;
		if (op(candidate, value)) {
			value = candidate;
			return true;
		}
		return false;
	}
};

template <typename T>
using Minimizer = Optimizer<T, std::less<T>>;

template <typename T>
using Maximizer = Optimizer<T, std::greater<T>>;

struct DebugCubeSet {
	DebugCubeSet() {
		mesh = Load7GM("data/meshes/debug-cube.7gm");
		auto vsh = LoadShader("debug_vs");
		auto fsh = LoadShader("debug_fs");
		program = bgfx::createProgram(vsh, fsh, true);
		uBaseColor = bgfx::createUniform("uBaseColor", bgfx::UniformType::Uniform4fv, 1);
	}

	~DebugCubeSet() {
		bgfx::destroyVertexBuffer(mesh.vbh);
		bgfx::destroyUniform(uBaseColor);
		bgfx::destroyProgram(program);
	}

	void Clear();
	void Add(Board::Coordinate pos, uint32_t tint);
	void Add(Board::Coordinate pos, float4 tint);
	void Draw();

private:
	struct Cube {
		Board::Coordinate pos;
		float4 color;
	};
	bgfx::ProgramHandle program;
	bgfx::UniformHandle uBaseColor;
	Mesh mesh;
	std::deque<Cube> cubes;
};


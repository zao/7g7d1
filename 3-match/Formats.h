#pragma once
#include <MathGeoLib/MathGeoLib.h>
#include <bgfx.h>

struct Vertex7GM {
	float3 pos;
	float3 n;
	float2 tc;
	uint32_t color;

	static void Init() {
		decl.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Normal, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.end();
	}

	static bgfx::VertexDecl decl;
};

struct Mesh
{
	uint32_t vertexCount;
	bgfx::VertexBufferHandle vbh;
};

Mesh Load7GM(std::string filepath);
void DestroyMesh(Mesh m);

bgfx::ShaderHandle LoadShader(std::string name);
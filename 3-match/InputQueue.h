#pragma once

#include <stdint.h>
#include <cassert>
#include <string>
#include <vector>

struct InputEvent {
	enum Type : uint16_t {
		MouseDown,
		MouseUp,
		MouseMove,
	};
	typedef uint16_t Size;

	Type type;
	Size size;
	char const* data;
};

struct InputQueue {
	std::vector<char> data;
	size_t count = 0;

	void push(InputEvent event) {
		push(event.type, event.size, event.data);
	}

	void push(InputEvent::Type type, uint16_t cb, void const* p) {
		auto header = std::make_pair(type, cb);
		data.insert(data.end(), (char const*)&header, (char const*)(&header + 1));
		auto q = (char const*)p;
		data.insert(data.end(), q, q + cb);
		++count;
	}

	size_t const TypeSize = sizeof(InputEvent::Type);
	size_t const SizeSize = sizeof(InputEvent::Size);
	size_t const HeaderSize = TypeSize + SizeSize;

	InputEvent front() const {
		size_t available = data.size();
		assert(available >= HeaderSize);
		InputEvent ret;
		memcpy(&ret.type, &data[0], TypeSize);
		memcpy(&ret.size, &data[TypeSize], SizeSize);
		assert(available >= HeaderSize + ret.size);
		ret.data = &data[HeaderSize];
		return ret;
	}

	void pop() {
		size_t available = data.size();
		assert(available >= HeaderSize);
		InputEvent::Size size;
		memcpy(&size, &data[TypeSize], SizeSize);
		assert(available >= HeaderSize + size);
		data.erase(data.begin(), data.begin() + HeaderSize + size);
		--count;
	}
};

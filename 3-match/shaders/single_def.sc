vec2 v_texcoord    : TEXCOORD0 = vec2(0.0, 0.0);
vec3 v_worldNormal : NORMAL    = vec3(0.0, 0.0, 1.0);
vec3 v_worldPos    : WORLDPOS  = vec3(0.0, 0.0, 1.0);

vec3 a_position : POSITION;
vec2 a_texcoord : TEXCOORD0;
vec3 a_normal   : NORMAL;
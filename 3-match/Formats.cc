#include "Formats.h"

#include <stdio.h>
#include <string>

bgfx::VertexDecl Vertex7GM::decl;

struct FileSlurp {
	explicit FileSlurp(std::string filepath) {
		file = fopen(filepath.c_str(), "rb");
		if (file) {
			fseek(file, 0, SEEK_END);
			byteCount = ftell(file);
			fseek(file, 0, SEEK_SET);
		}
	}

	~FileSlurp() {
		if (file) {
			fclose(file);
		}
	}

	bool IsReady() const { return !!file; }
	size_t ByteCount() const { return byteCount; }

	void Read(size_t offset, void* data, size_t amount) {
		fseek(file, (long)offset, SEEK_SET);
		fread(data, 1, amount, file);
	}

private:
	FileSlurp(FileSlurp const&) = delete;
	FileSlurp& operator = (FileSlurp const&) = delete;

	FILE* file;
	size_t byteCount = 0;
};

Mesh Load7GM(std::string filepath) {
	Mesh ret = {};
	FileSlurp slurp(filepath);
	if (!slurp.IsReady() || slurp.ByteCount() < 8) {
		return ret;
	}

	char sig[4] = {};
	slurp.Read(0, sig, 4);
	if (memcmp(sig, "7GM", 3) != 0 || sig[3] != 0) return ret;

	uint32_t vertexCount = 0;
	slurp.Read(4, &vertexCount, sizeof(uint32_t));
	uint32_t const VertexByteCount = vertexCount * sizeof(Vertex7GM);
	if (VertexByteCount != slurp.ByteCount() - 8) return ret;

	bgfx::Memory const* mem = bgfx::alloc(VertexByteCount);
	slurp.Read(8, mem->data, mem->size);

	ret.vertexCount = vertexCount;
	ret.vbh = bgfx::createVertexBuffer(mem, Vertex7GM::decl);
	return ret;
}

void DestroyMesh(Mesh m) {
	bgfx::destroyVertexBuffer(m.vbh);
}

static std::string ShaderSubdir() {
    switch (bgfx::getRendererType()) {
    case bgfx::RendererType::Direct3D9: return "dx9";
    case bgfx::RendererType::Direct3D11:
    case bgfx::RendererType::Direct3D12: return "dx11";
    case bgfx::RendererType::OpenGL: return "glsl";
    case bgfx::RendererType::OpenGLES: return "gles";
    default: abort();
	}
}

bgfx::ShaderHandle LoadShader(std::string name) {
	bgfx::ShaderHandle ret = {};
	std::string subdir;
	std::string filepath = "data/shaders/" + ShaderSubdir() + "/" +
		name + ".bin";
	FileSlurp slurp(filepath);
	if (!slurp.IsReady() || slurp.ByteCount() == 0)
		return ret;
	bgfx::Memory const* mem = bgfx::alloc((uint32_t)slurp.ByteCount());
	slurp.Read(0, mem->data, mem->size);
	ret = bgfx::createShader(mem);
	return ret;
}

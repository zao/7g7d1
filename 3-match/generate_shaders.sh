#!/usr/bin/env bash
SHADERC=${HOME}/bounce/bgfx/.build/linux64_gcc/bin/shadercRelease
mkdir -p data/shaders/dx11
mkdir -p data/shaders/glsl
$SHADERC -I shaders --platform linux --varyingdef shaders/single_def.sc --profile 120 --type vertex -f shaders/single_vs.sc -o data/shaders/glsl/single_vs.bin
$SHADERC -I shaders --platform linux --varyingdef shaders/single_def.sc --profile 120 --type frag   -f shaders/single_fs.sc -o data/shaders/glsl/single_fs.bin

CXXFLAGS=-std=gnu++11

CPPFLAGS="\
	-I${HOME}/bounce/bx/include \
	-I${HOME}/bounce/bgfx/include \
	-I${HOME}/opt/MathGeoLib-git/include \
	-I${HOME}/opt/SDL2-2.0.3/include \
	-D__STDC_LIMIT_MACROS=1 \
	-D__STDC_FORMAT_MACROS=1 \
	-D__STDC_CONSTANT_MACROS=1 \
"

LDFLAGS="\
	-L${HOME}/bounce/bgfx/.build/linux64_gcc/bin \
	-L${HOME}/opt/MathGeoLib-git/lib \
	-L${HOME}/opt/SDL2-2.0.3/lib \
	-Wl,-rpath -Wl,${HOME}/opt/SDL2-2.0.3/lib \
"

LIBS="\
-lbgfxRelease \
-lMathGeoLib \
-lSDL2 -lX11 -lGL \
"

g++ ${CXXFLAGS} ${CPPFLAGS} ${LDFLAGS} \
	-o bin/3-match Formats.cc MatchMain.cc \
	${LIBS}

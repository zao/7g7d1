#ifndef NOMINMAX
#define NOMINMAX
#endif
#include "SDL2/SDL.h"
#include "SDL2/SDL_syswm.h"
#include "SDL2_mixer/SDL_mixer.h"
#ifdef None
#undef None
#endif
#include <bgfx.h>
#include <bgfxplatform.h>
#include <bx/timer.h>
#include <MathGeoLib/MathGeoLib.h>

#include <functional>
#include <iostream>
#include <map>
#include <vector>

#include <boost/variant.hpp>

#include "Formats.h"
#include "CoordinateFrame.h"
#include "InputQueue.h"

int ZMain();

#ifdef WIN32
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	return ZMain();
}
#else
void OutputDebugStringA(char const* s) {
	std::cerr << s;
}

int main() {
	return ZMain();
}
#endif

typedef int64_t FrameCount;
typedef int64_t FrameNumber;

namespace tuning {
	FrameCount const SimFramesPerSecond = 720;
	FrameCount const FallFramesPerTile = 10;
	size_t const HorizontalTileCount = 6;
	size_t const VerticalTileCount = 5;
	size_t const TileKindCount = 4;
	float const ViewWidth = 1280.0f;
	float const ViewHeight = 720.0f;
	float const ViewAspect = ViewWidth / ViewHeight;
	float const TileExtent = 15.0f;
}

struct Board {
	struct Tile {
		bool exists = true;
		FrameCount framesFalling = 0;
		bool resting = true;
		uint8_t colour = 0;
		bool powered = false;
	};

	Board() {
		tiles.resize(tuning::HorizontalTileCount * tuning::VerticalTileCount);
	}

	struct Location {
		size_t row, col;

		bool operator == (Location rhs) const {
			return row == rhs.row && col == rhs.col;
		}

		bool operator != (Location rhs) const {
			return !(*this == rhs);
		}

		bool operator < (Location rhs) const {
			if (row != rhs.row) return row < rhs.row;
			return col < rhs.col;
		}

		bool AlignedWith(Location other) const {
			return row == other.row || col == other.col;
		}

		bool InAlignedClosedRange(Location from, Location to) const {
			if (!from.AlignedWith(to) || !AlignedWith(from) || !AlignedWith(to)) {
				return false;
			}
			size_t v;
			std::pair<size_t, size_t> range;
			if (row == from.row) {
				v = col;
				range = std::minmax(from.col, to.col);
			}
			else if (col == from.col) {
				v = row;
				range = std::minmax(from.row, to.row);
			}
			return v >= range.first && v <= range.second;
		}

		Location MovedAlignedToward(Location target, int amount) const {
			if (!AlignedWith(target)) {
				return *this;
			}
			auto const SignOrZero = [](int v) -> int {
				if (v > 0) return 1;
				if (v == 0) return 0;
				return -1;
			};
			int sRow = SignOrZero((int)target.row - (int)row);
			int sCol = SignOrZero((int)target.col - (int)col);
			Location ret = {
				row + amount * sRow,
				col + amount * sCol,
			};
			return ret;
		}
	};

	// NOTE(zao): rows increasing along +Y
	Tile& GetTile(size_t row, size_t col) {
		return tiles.at(row * tuning::HorizontalTileCount + col);
	}

	Tile& GetTile(Location loc) {
		return GetTile(loc.row, loc.col);
	}

	Location GetOffsetLocation(Location loc) {
		if (!moveStart || !moveEnd) {
			return loc;
		}
		if (loc.InAlignedClosedRange(*moveStart, *moveEnd)) {
			if (loc == *moveStart) {
				return *moveEnd;
			}
			return loc.MovedAlignedToward(*moveStart, 1);
		}
		return loc;
	}

	bool ValidLocation(size_t row, size_t col) {
		return (row < tuning::VerticalTileCount && col < tuning::HorizontalTileCount);
	}

	void BeginMove(Location loc) {
		moveStart.reset(new Location(loc));
		UpdateMove(&loc);
	}

	void UpdateMove(Location const* loc) {
		if (loc) {
			if (loc->AlignedWith(*moveStart)) {
				moveEnd.reset(new Location(*loc));
			}
		}
		else {
			moveEnd.reset();
		}
	}

	bool CommitMove() {
		bool isValidMove = false;
		if (moveEnd) {
			Tile& from = GetTile(*moveStart);
			Tile& to = GetTile(*moveEnd);
			std::swap(from, to);
			isValidMove = MatchParticipant(*moveStart) || MatchParticipant(*moveEnd);
			if (!isValidMove) {
				std::swap(from, to);
			}
		}
		moveStart.reset();
		moveEnd.reset();
		return isValidMove;
	}

	bool MatchParticipant(Location loc) {
		return MatchParticipant(loc.row, loc.col);
	}

	bool MatchParticipant(size_t row, size_t col) {
		if (!ValidLocation(row, col)) {
			return false;
		}
		Tile midTile = GetTile(row, col);
		if (!midTile.resting) {
			return false;
		}
		Location candidates[][4] = {
			{ Location{ row, col-2 }, Location{ row, col-1 }, Location{ row, col+1 }, Location{ row, col+2 }, },
			{ Location{ row-2, col }, Location{ row-1, col }, Location{ row+1, col }, Location{ row+2, col }, },
		};
		for (size_t setIndex = 0; setIndex < 2; ++setIndex) {
			// 0 1 X 2 3
			bool matches[4] = {};
			for (size_t i = 0; i < 4; ++i) {
				Location loc = candidates[setIndex][i];
				if (ValidLocation(loc.row, loc.col)) {
					Tile tile = GetTile(loc.row, loc.col);
					if (tile.resting && tile.colour == midTile.colour) {
						matches[i] = true;
					}
				}
			}
			if (matches[0] && matches[1] || matches[1] && matches[2] || matches[2] && matches[3]) {
				return true;
			}
		}
		return false;
	}

	std::vector<Tile> tiles;
	std::shared_ptr<Location> moveStart;
	std::shared_ptr<Location> moveEnd;
};

Board GenerateBoard(LCG& lcg) {
	Board ret;
	bool goodBoard;
	do {
		goodBoard = true;
		for (auto& tile : ret.tiles) {
			tile.colour = lcg.Int(0, tuning::TileKindCount-1);
		}
		for (size_t row = 0; row < tuning::VerticalTileCount; ++row) {
			for (size_t col = 0; col < tuning::HorizontalTileCount; ++col) {
				Board::Tile& tile = ret.GetTile(row, col);
				bool matching = false;
				size_t originalColour = tile.colour;
				for (size_t i = 1; i <= tuning::TileKindCount; ++i) {
					matching = ret.MatchParticipant(row, col);
					if (matching) {
						tile.colour = (originalColour+i)%tuning::TileKindCount;
					}
				}
			}
		}
		for (size_t row = 0; row < tuning::VerticalTileCount; ++row) {
			for(size_t col = 0; col < tuning::HorizontalTileCount; ++col) {
				if (ret.MatchParticipant(row, col)) {
					goodBoard = false;
				}
			}
		}
	} while (!goodBoard);
	return ret;
}

struct Game {
	struct FrameState {
		Board board;
		enum Mode {
			Idling,
			Selected,
			Resolving,
			Undoing,
			GameOver,
		};
		struct Interact {
			Interact()
				: mode(Idling)
				, nextMode(mode)
			{}
			Mode mode;
			Mode nextMode;

			struct Idle {
			};
			struct Select {
				Board::Location source, target;
			};
			struct Resolve {
				Board::Location source, target;
			};
			struct Undo {
			};
			struct Over {
			};
			boost::variant<Idle, Select, Resolve, Undo, Over> data;
		} interact;
	};

	void Reset(uint32_t seed) {
		gameLCG.Seed(seed);
		boardLCG.Seed(gameLCG.IntFast());
		state.board = GenerateBoard(boardLCG);
		state.interact.mode = FrameState::Idling;
		frameNumber = 0;
	}

	LCG gameLCG;
	LCG boardLCG;
	FrameState state;
	FrameNumber frameNumber;

	FrameState& State() { return state; }
	FrameState& RenderState(float f) { return state; }

	void AdvanceFrame() {
		++frameNumber;
	}
};

static char const* const GameStateNames[] = {
	"Idling",
	"Selected",
	"Resolving",
	"Undoing",
	"GameOver",
};

struct BoardVis {
	CoordinateFrame frame;
	float3 X = float3::unitX;
	float3 Y = float3::unitY;
	float3 Z = float3::unitZ;
	float3 midWC = float3(0.0f, 0.0f, 0.0f);
	float2 const MidTC = float2((float)(tuning::HorizontalTileCount) / 2.0f,
	                            (float)(tuning::VerticalTileCount  ) / 2.0f);
	float2 const Extent = float2(tuning::TileExtent);

	BoardVis() {
		frame.X = float3::unitX;
		frame.Y = float3::unitY;
		frame.Z = float3::unitZ;
		float3 originWC = midWC;
		originWC -= (MidTC.x) * X * Extent.x;
		originWC -= (MidTC.y) * Y * Extent.y;
		frame.origin = originWC;
	}

	float3 PieceTranslation(Board::Location location) {
		float3 lc = float3(Extent.Mul(float2((float)location.col, (float)location.row) + float2(0.5f + 0.5f)), 0.0f);
		float3 pieceWC = MapFromFrame(frame, lc);
		return pieceWC;
	}

	Quat PieceOrientation(Board::Location location) {
		static LCG lcg(9001u);
		static std::map<Board::Location, Quat> rotations;
		auto I = rotations.find(location);
		if (I == rotations.end()) {
			float const RotationImpactFactor = 0.25f;
			Quat q = Quat::identity.Slerp(Quat::RandomRotation(lcg), RotationImpactFactor);
			I = rotations.insert(rotations.end(), std::make_pair(location, q));
		}
		return I->second;
	}

	OBB PieceBounds(Board::Location location) {
		auto mid = PieceTranslation(location);
		OBB ret = OBB(mid, float3(tuning::TileExtent, tuning::TileExtent, tuning::TileExtent) / 2.0f, X, Y, Z);
		return ret;
	}

	float3 OriginWC() const {
		float3 originWC = midWC;
		originWC -= (MidTC.x) * X * Extent.x;
		originWC -= (MidTC.y) * Y * Extent.y;
		return originWC;
	}

	bool HitTest(Ray r, Board::Location* bestTarget) {
		Board::Location bestHit;
		float bestD = std::numeric_limits<float>::max();
		bool hasHit = false;
		for (size_t row = 0; row < tuning::VerticalTileCount; ++row) {
			for (size_t col = 0; col < tuning::HorizontalTileCount; ++col) {
				Board::Location loc = { row, col };
				OBB bb = PieceBounds(loc);
				float nearD, farD;
				if (r.Intersects(bb, nearD, farD) && nearD >= 0.0f && nearD < bestD) {
					if (!bestTarget) {
						return true;
					}
					bestHit = loc;
					bestD = nearD;
					hasHit = true;
				}
			}
		}
		if (hasHit) {
			*bestTarget = bestHit;
		}
		return hasHit;
	}
};

int ZMain() {
	SDL_Init(SDL_INIT_NOPARACHUTE | SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	SDL_DisplayMode mode = {};
	SDL_GetCurrentDisplayMode(0, &mode);
	int const WindowWidth = mode.w;
	int const WindowHeight = mode.h;

	Mix_Init(0);
	Mix_OpenAudio(22050, AUDIO_S16LSB, 1, 1024);
	Mix_AllocateChannels(16);

	auto const& MapPixelCoord = [WindowWidth, WindowHeight](int winX, int winY) -> float2 {
		float x = winX + 0.5f;
		float y = winY + 0.5f;
		float2 ret = float2(
			Lerp(-1.0f, 1.0f, x / WindowWidth),
			Lerp(1.0f, -1.0f, y / WindowHeight));
		return ret;
	};

	uint32_t windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS;
	auto window = SDL_CreateWindow("3-match",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		WindowWidth, WindowHeight,
		windowFlags);
	bgfx::sdlSetWindow(window);
	bgfx::init();
	uint32_t const ResetFlags = BGFX_RESET_MSAA_X16 | BGFX_RESET_VSYNC;
	uint32_t const DebugFlags = BGFX_DEBUG_TEXT;
	bgfx::reset(WindowWidth, WindowHeight, ResetFlags);
	bgfx::setDebug(DebugFlags);
	bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030FF);

	Vertex7GM::Init();

	Game game;
	game.Reset(9001u);

	Mix_Chunk* sndIllegalMove = Mix_LoadWAV("data/sfx/illegal-move.wav");
	Mix_Chunk* sndValidMove = Mix_LoadWAV("data/sfx/valid-move.wav");

	Mesh tokenMeshes[] = {
		Load7GM("data/meshes/box-token.7gm"),
		Load7GM("data/meshes/cylinder-token.7gm"),
		Load7GM("data/meshes/torusknot-token.7gm"),
		Load7GM("data/meshes/hedra-token.7gm"),
	};
	bgfx::ProgramHandle singleProgram;
	bgfx::UniformHandle uLightCount;
	bgfx::UniformHandle uLightPs;
	bgfx::UniformHandle uLightCIs;
	{
		auto vsh = LoadShader("single_vs");
		auto fsh = LoadShader("single_fs");
		singleProgram = bgfx::createProgram(vsh, fsh, true);
		uLightCount = bgfx::createUniform("u_lightCount", bgfx::UniformType::Uniform1iv, 1);
		uLightPs = bgfx::createUniform("u_worldLight", bgfx::UniformType::Uniform4fv, 16);
		uLightCIs = bgfx::createUniform("u_lightColourIntensity", bgfx::UniformType::Uniform4fv, 16);
	}

	Frustum frustum;
	frustum.SetKind(FrustumSpaceD3D, FrustumRightHanded);
	frustum.SetViewPlaneDistances(0.1f, 2000.0f);

	frustum.SetVerticalFovAndAspectRatio(pi/4.0f, (float)WindowWidth / WindowHeight);
	{
		float3 eye = float3(0.0f, 10.0f, 150.0f);
		float3 at = float3::zero;
		float3x4 W = float3x4::LookAt(eye, at, -float3::unitZ, float3::unitY, float3::unitY);
		frustum.SetWorldMatrix(W);
	}

	BoardVis boardVis;
	boardVis.X = float3::unitX;
	boardVis.Y = float3::unitY;
	boardVis.Z = float3::unitZ;
	boardVis.midWC = float3(0.0f, 0.0f, 0.0f);
	
	std::string lastMouseDebugInfo;

	int64_t const WallTickFrequency = bx::getHPFrequency();
	int64_t lastWallTicks = bx::getHPCounter();

	int64_t const TicksPerSimFrame = (int64_t)(WallTickFrequency / tuning::SimFramesPerSecond);
	int64_t simTickPool = 0;
	int64_t simFramesElapsed = 0;

	InputQueue inputs;
	bool running = true;
	while (running) {
		bool wantMouseAtResult = false;
		SDL_Event event = {};
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE) {
				running = false;
			}
			if (event.type == SDL_QUIT) {
				running = false;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
				float2 pos = MapPixelCoord(event.button.x, event.button.y);
				inputs.push(InputEvent::MouseDown, sizeof(float2), pos.ptr());
			}
			if (event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_LEFT) {
				float2 pos = MapPixelCoord(event.button.x, event.button.y);
				inputs.push(InputEvent::MouseUp, sizeof(float2), pos.ptr());
			}
			if (event.type == SDL_MOUSEMOTION) {
				float2 pos = MapPixelCoord(event.motion.x, event.motion.y);
				inputs.push(InputEvent::MouseMove, sizeof(float2), &pos);
			}
		}
		if (!running) {
			break;
		}

		// NOTE(zao): Simulating
		int64_t wallTicks = bx::getHPCounter();
		int64_t newTicks = wallTicks - lastWallTicks;
		int64_t const MaxTicksToSim = (int64_t)(0.25f * WallTickFrequency);
		if (newTicks > MaxTicksToSim)
			newTicks = MaxTicksToSim;
		lastWallTicks = wallTicks;

		float2 dragSource;

		simTickPool += newTicks;
		while (simTickPool >= TicksPerSimFrame) {
			auto& state = game.State();
			switch (state.interact.mode) {
				case Game::FrameState::GameOver: {
				} break;
				case Game::FrameState::Idling: {
					if (inputs.count) {
						auto e = inputs.front();
						switch (e.type) {
							case InputEvent::MouseUp: {
								float2 pos;
								memcpy(&pos, e.data, sizeof(float2));
								Ray r = frustum.UnProject(pos);
								Board::Location hitLoc;
								if (boardVis.HitTest(r, &hitLoc)) {
									state.interact.nextMode = Game::FrameState::Selected;
									Game::FrameState::Interact::Select select = { hitLoc, hitLoc };
									state.interact.data = select;
									state.board.BeginMove(hitLoc);
								}
							} break;
							case InputEvent::MouseMove: {
							} break;
						}
						inputs.pop();
					}
				} break;
				case Game::FrameState::Selected: {
					auto& select = boost::get<Game::FrameState::Interact::Select>(state.interact.data);
					if (inputs.count) {
						auto e = inputs.front();
						switch (e.type) {
							case InputEvent::MouseUp: {
								float2 pos;
								memcpy(&pos, e.data, sizeof(float2));
								Ray r = frustum.UnProject(pos);
								Board::Location hitLoc;
								bool commitSuccess = false;
								if (boardVis.HitTest(r, &hitLoc)) {
									if (hitLoc != select.source) {
										state.board.UpdateMove(&hitLoc);
										commitSuccess = state.board.CommitMove();
										state.interact.nextMode = Game::FrameState::Resolving;
										Game::FrameState::Interact::Resolve resolve = { select.source, hitLoc };
										state.interact.data = resolve;
									}
									else {
										state.board.UpdateMove(nullptr);
										commitSuccess = state.board.CommitMove();
										state.interact.nextMode = Game::FrameState::Undoing;
									}
									Mix_Chunk* moveSound = commitSuccess ? sndValidMove : sndIllegalMove;
									Mix_PlayChannel(-1, moveSound, 0);
								}
								else {
									state.interact.nextMode = Game::FrameState::Undoing;
								}
							} break;
							case InputEvent::MouseMove: {
								float2 pos;
								memcpy(&pos, e.data, sizeof(float2));
								Ray r = frustum.UnProject(pos);
								Board::Location hitLoc;
								if (boardVis.HitTest(r, &hitLoc)) {
									select.target = hitLoc;
									state.board.UpdateMove(&hitLoc);
								}
								else {
									state.board.UpdateMove(nullptr);
								}
							} break;
						}
						inputs.pop();
					}
				} break;
				case Game::FrameState::Resolving: {
					bool done = true;
					if (done) {
						state.interact.nextMode = Game::FrameState::Idling;
					}
				} break;
				case Game::FrameState::Undoing: {
					bool done = true;
					if (done) {
						state.interact.nextMode = Game::FrameState::Idling;
					}
				} break;
			}
			++simFramesElapsed;
			simTickPool -= TicksPerSimFrame;
			state.interact.mode = state.interact.nextMode;
			game.AdvanceFrame();
		}

		// NOTE(zao): Drawing
		int64_t const TicksSincePreviousSimFrame = ((simTickPool + TicksPerSimFrame - 1) % (TicksPerSimFrame));
		float renderF = TicksSincePreviousSimFrame / (float)(TicksPerSimFrame - 1);
		auto& renderState = game.RenderState(renderF);
		bgfx::setViewRect(0, 0, 0, WindowWidth, WindowHeight);
		{
			float4x4 const ViewMatrix = frustum.ViewMatrix();
			float4x4 const ProjectionMatrix = frustum.ProjectionMatrix();
			bgfx::setViewTransform(0, ViewMatrix.Transposed().ptr(), ProjectionMatrix.Transposed().ptr());
		}
		bgfx::submit(0);

		bgfx::dbgTextClear();
		bgfx::dbgTextPrintf(0, 0, 0x3F, "%s", lastMouseDebugInfo.c_str());
		bgfx::dbgTextPrintf(0, 2, 0x3F, "%d events", inputs.count);
		{
			char const* stateStr = GameStateNames[renderState.interact.mode];
			bgfx::dbgTextPrintf(0, 3, 0x3F, "State: %s", stateStr);
			if (renderState.interact.mode == Game::FrameState::Selected) {
				auto sel = boost::get<Game::FrameState::Interact::Select>(renderState.interact.data);
				bgfx::dbgTextPrintf(0, 4, 0x3F, "Selection: r%dc%d -> r%dc%d",
					sel.source.row, sel.source.col, sel.target.row, sel.target.col);
			}
		}

		int lightCount = 1;
		float4 lightPositions[16] = {
			float4(0.0f, 50.0f, 50.0f, 0.0f),
		};
		float4 lightAttributes[16] = {
			float4(float3(1.0f, 1.0f, 1.0f), 500.0f),
		};

		auto const& DrawPiece = [&](Board::Location loc, uint8_t colour, Quat R) {
			bgfx::setProgram(singleProgram);
			bgfx::setUniform(uLightCount, &lightCount, 1);
			bgfx::setUniform(uLightPs, lightPositions, 16);
			bgfx::setUniform(uLightCIs, lightAttributes, 16);
			float3x4 T = float3x4::Translate(boardVis.PieceTranslation(loc));
			float4x4 M = T * R;
			bgfx::setVertexBuffer(tokenMeshes[colour%4].vbh);
			bgfx::setTransform(M.Transposed().ptr());
			bgfx::setState(BGFX_STATE_DEFAULT);
			bgfx::submit(0);
		};

		uint8_t selectedTileKind = 0;

		size_t const Rows = tuning::VerticalTileCount;
		for (size_t row = 0; row < Rows; ++row) {
			size_t const Columns = tuning::HorizontalTileCount;
			for (size_t col = 0; col < Columns; ++col) {
				auto tile = renderState.board.GetTile(row, col);
				if (!tile.exists)
					continue;
				auto colour = tile.colour;
				Board::Location loc = { row, col };
				Quat R = boardVis.PieceOrientation(loc);
				Board::Location renderLoc = renderState.board.GetOffsetLocation(loc);
				DrawPiece(renderLoc, colour, R);
			}
		}

		bgfx::frame();
	}

	SDL_DestroyWindow(window);
	Mix_CloseAudio();
	while (Mix_Init(0)) {
		Mix_Quit();
	}
	SDL_Quit();
	return 0;
}

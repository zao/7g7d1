#pragma once
#include <MathGeoLib/MathGeoLib.h>

struct CoordinateFrame {
	float3 X, Y, Z;
	float3 origin;
	float3 extents;
};

float3 MapToFrame(CoordinateFrame frame, float3 worldPoint) {
	float3 worldOffset = worldPoint - frame.origin;
	float3 framePoint = float3(
		Dot(frame.X, worldOffset),
		Dot(frame.Y, worldOffset),
		Dot(frame.Z, worldOffset));
	return framePoint;
}

float3 MapFromFrame(CoordinateFrame frame, float3 framePoint) {
	float3 worldOffset =
		frame.X * framePoint.x +
		frame.Y * framePoint.y +
		frame.Z * framePoint.z;
	float3 worldPoint = worldOffset + frame.origin;
	return worldPoint;
}

float3 ClosestPointOnFrame(CoordinateFrame frame, float3 worldPoint) {
	float3 framePoint = MapToFrame(frame, worldPoint);
	AABB bb = AABB(float3::zero, frame.extents);
	float3 closestWorldPoint = MapFromFrame(frame, bb.ClosestPoint(framePoint));
	return closestWorldPoint;
}

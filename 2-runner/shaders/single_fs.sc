$input v_color0, v_worldNormal, v_worldPos

#include <bgfx_shader.sh>

uniform int u_lightCount;
uniform vec4 u_worldLight[16];
uniform vec4 u_lightColourIntensity[16];

void main()
{
	vec3 N = normalize(v_worldNormal);
	vec3 aboveColor = vec3(0.7f, 0.7f, 0.9f);
	vec3 belowColor = vec3(0.7f, 0.7f, 0.6f);
	vec3 diffuse = vec3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < u_lightCount; ++i) {
		vec3 lightVec = u_worldLight[i].xyz - v_worldPos;
		vec3 L = normalize(lightVec);
		float NdotL = saturate(dot(L, N));
		float dist = sqrt(dot(lightVec, lightVec));
		float falloff = 1.0f / (1.0 + 0.1*dist + 0.2*dist*dist);
		vec3 lightCol = u_lightColourIntensity[i].xyz;
		float lightInt = u_lightColourIntensity[i].w;
		float3 contrib = saturate(NdotL * falloff * lightInt * lightCol);
		diffuse += contrib;
	}
	float w = 0.5f * (1.0f + dot(vec3(0.0f, 1.0f, 0.0f), N));
	vec3 hemiLight = mix(belowColor, aboveColor, w);
	gl_FragColor = vec4(diffuse * v_color0 * hemiLight, 1.0f);
}

$input a_position, a_color0, a_normal
$output v_color0, v_worldNormal, v_worldPos

#include <bgfx_shader.sh>

void main()
{
	vec4 pos = vec4(a_position, 1.0);
	gl_Position = mul(u_modelViewProj, pos);
	v_color0 = a_color0;
	mat3 normalMatrix = transpose(transpose((mat3)u_model[0])); // assume uniform scale
	v_worldNormal = mul(normalMatrix, a_normal);
	v_worldPos = mul(u_model[0], pos).xyz;
}

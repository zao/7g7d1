#define NOMINMAX 1
#include <algorithm>
using std::min; using std::max;
#include <Windows.h>
#include <atlbase.h>
#include <atlapp.h>
#include <atltypes.h>
#include <atlctl.h>
#include <atlcrack.h>
#include <atlframe.h>
#include <atlwin.h>
#include <bx/timer.h>
#include <bgfx.h>
#include <bgfxplatform.h>
#include <MathGeoLib/MathGeoLib.h>

#include <list>
#include <map>
#include <set>

namespace WTL { using ::CPoint; }

struct SlurpResult {
	std::vector<char> data;

	operator bgfx::Memory const* () const {
		uint32_t n = (uint32_t)data.size();
		assert(n == data.size());
		return bgfx::copy(data.data(), n);
	}
};

std::string RendererName() {
	switch (bgfx::getRendererType()) {
	case bgfx::RendererType::Direct3D9: return "dx9";
	case bgfx::RendererType::Direct3D11:
	case bgfx::RendererType::Direct3D12: return "dx11";
	case bgfx::RendererType::OpenGL: return "glsl";
	case bgfx::RendererType::OpenGLES: return "gles";
	default: abort();
	}
}

SlurpResult SlurpAsset(std::string filepath) {
	HANDLE h = CreateFileA(filepath.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_DELETE, 0, OPEN_ALWAYS, 0, 0);
	if (!h) {
		return SlurpResult{};
	}
	LARGE_INTEGER cb = {};
	GetFileSizeEx(h, &cb);
	if (cb.HighPart) {
		CloseHandle(h);
		return SlurpResult{};
	}
	DWORD n = cb.LowPart;
	SlurpResult ret;
	ret.data.resize(n);
	DWORD nRead = 0;
	BOOL res = ReadFile(h, ret.data.data(), n, &nRead, 0);
	CloseHandle(h);
	if (!res || nRead != n) {
		return SlurpResult{};
	}
	return ret;
}

bgfx::ShaderHandle LoadShader(std::string name) {
	std::string renderer = RendererName();
	std::string filepath = "data\\shaders\\" + renderer + "\\" + name + ".bin";
	auto bin = SlurpAsset(filepath);
	return bgfx::createShader(bin);
}

struct GameWindow : WTL::CFrameWindowImpl<GameWindow>
{
	BEGIN_MSG_MAP(GameWindow)
		MSG_WM_DESTROY([]{ PostQuitMessage(0); })
		MSG_WM_KEYDOWN(OnKeyDown)
		MSG_WM_KEYUP(OnKeyUp)
		MSG_WM_MOUSEWHEEL(OnMouseWheel)
	END_MSG_MAP()

	void OnKeyDown(UINT key, UINT rep, UINT flags) {
		if (key == VK_SPACE && !spaceHeld) {
			spaceHeld = true;
			wantJump = true;
		}
	}

	void OnKeyUp(UINT key, UINT rep, UINT flags) {
		if (key == VK_ESCAPE) {
			DestroyWindow();
		}
		if (key == 'R') {
			wantReset = true;
		}
		if (key == VK_SPACE) {
			spaceHeld = false;
			wantJump = false;
		}
	}

	BOOL OnMouseWheel(UINT flags, short delta, CPoint pt) {
		twiddleKnob -= (float)delta / WHEEL_DELTA;
		return TRUE;
	}

	void CreateCentered(float2 size) {
		float const aspect = size.x / size.y;
		CPoint zero = {};
		HMONITOR primaryMonitor = MonitorFromPoint(zero, MONITOR_DEFAULTTOPRIMARY);
		MONITORINFO mi = {};
		mi.cbSize = sizeof(mi);
		GetMonitorInfo(primaryMonitor, &mi);
		CRect workArea = mi.rcWork;
		LONG w = (LONG)(0.75f * workArea.Width());
		LONG h = (LONG)(w / aspect);
		CRect windowBox = {};
		windowBox.left = workArea.CenterPoint().x - w/2;
		windowBox.top = workArea.CenterPoint().y - h/2;
		windowBox.right = windowBox.left + w;
		windowBox.bottom = windowBox.top + h;
		AdjustWindowRect(&windowBox, GetWndStyle(0), FALSE);
		Create(0, windowBox);
	}

	bool wantJump = false;
	bool spaceHeld = false;
	float twiddleKnob = 0.0f;
	bool wantReset = false;
};

struct Vertex {
	float3 pos;
	float3 n;
	float2 tc;
	uint32_t color;

	static void Init() {
		decl.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Normal, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.end();
	}

	static bgfx::VertexDecl decl;
};

bgfx::VertexDecl Vertex::decl;

struct Mesh
{
	uint32_t vertexCount;
	bgfx::VertexBufferHandle vbh;
};

static Mesh slideLeft, slideMid, slideRight;
static Mesh ship;

Mesh Load7GM(std::wstring filepath) {
	Mesh ret = {};
	HANDLE h = CreateFile(filepath.c_str(), GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
	if (h == INVALID_HANDLE_VALUE) return ret;

	uint64_t cb = 0;
	GetFileSizeEx(h, (LARGE_INTEGER*)&cb);
	DWORD nRead = 0;
	char sig[4] = {};
	ReadFile(h, sig, 4, &nRead, 0);
	if (nRead != 4 || memcmp(sig, "7GM", 3) != 0 || sig[3] != 0) return ret;

	uint32_t vertexCount = 0;
	ReadFile(h, &vertexCount, sizeof(uint32_t), &nRead, 0);
	uint32_t const VertexByteCount = vertexCount * sizeof(Vertex);
	if (nRead != 4 || VertexByteCount != cb - 4 - 4) return ret;

	bgfx::Memory const* mem = bgfx::alloc(VertexByteCount);
	ReadFile(h, mem->data, mem->size, &nRead, 0);
	CloseHandle(h);

	ret.vertexCount = vertexCount;
	ret.vbh = bgfx::createVertexBuffer(mem, Vertex::decl);
	return ret;
}

void DestroyMesh(Mesh m) {
	bgfx::destroyVertexBuffer(m.vbh);
}

typedef int64_t FrameNumber;

static float const TimeScale = 1.0f;
static float2 const InitialPlayerPosition = float2(-65.0f, 0.0f);
static float const InitialPlayerSpeed = 75.0f;
static float const SegmentLength = 10.0f;
static float const BoostAcceleration = 10.0f;
static float const JumpVelocity = 40000.0f;
static float const FallVelocity = -60000.0f;
static float const FramesPerSecond = 720.0f;
static float const EjectRate = 100.0f;
static float const EjectDepth = 1.0f;
static float const PlayerWidth = 10.0f;
static float const CameraDistanceFactor = 1000.0f;
static float const CameraFollowFactor = 0.01f;
static float const CameraLookAhead = 40.0f;
static float const ScorePerSpeedTick = 50.0f;
static FrameNumber const MinJumpFrameCount = (FrameNumber)(0.05f * FramesPerSecond);
static FrameNumber const MaxJumpFrameCount = (FrameNumber)(0.5f * FramesPerSecond);

struct Game
{
	Game()
		: frame(0)
	{
	}

	struct Span
	{
		uint64_t id;
		float2 pos;
		size_t chunks;
		float boostAmount;
	};

	struct SpanLight
	{
	};

	struct Board
	{
		LCG spanLCG;
		uint64_t nextSpanId = 1;
	};

	struct FrameState
	{
		FrameState() {
			spans.push_back(Span{ 0, InitialPlayerPosition, 7, 20.0f });
		}
		float2 playerPos = InitialPlayerPosition;
		float2 playerVel = float2(0.0f, 0.0f);
		float2 cameraPos = playerPos;
		float playerSpeed = InitialPlayerSpeed;
		float bestPlayerPos = playerPos.x;
		bool boosting = false;
		float boostAmount = 0.0f;
		bool jumping = false;
		bool falling = true;
		bool dead = false;
		FrameNumber playerDeathFrame = 0;
		uint64_t score = 0;
		FrameNumber jumpFramesDone = 0;
		std::vector<Span> spans;
	};

	void AdvanceFrame() {
		++frame;
		State() = LastState();
	}

	FrameState& State() { return state[frame%2]; }
	FrameState const& LastState() const { return state[(frame+1)%2]; }

	void Reset() {
		board = Board();
		FrameState s;
		state[0] = s;
		state[1] = s;
		frame = 0;
	}

	FrameState const& ResolveState(float f) {
		auto& foo = LastState();
		auto& bar = State();
		lerped = FrameState();
#define LERP(Value) lerped.Value = Lerp(foo.Value, bar.Value, f)
#define ASSIGN(Value) lerped.Value = bar.Value
		LERP(playerPos);
		LERP(playerVel);
		LERP(cameraPos);
		LERP(playerSpeed);
		LERP(bestPlayerPos);
		ASSIGN(boosting);
		LERP(boostAmount);
		ASSIGN(jumping);
		ASSIGN(falling);
		ASSIGN(dead);
		ASSIGN(playerDeathFrame);
		ASSIGN(score);
		ASSIGN(jumpFramesDone);
		{
			lerped.spans.reserve(std::max(foo.spans.size(), bar.spans.size()));
			auto fooI = foo.spans.begin(), fooEnd = foo.spans.end();
			auto barI = bar.spans.begin(), barEnd = bar.spans.end();
			while (1) {
				Span mix = {};
				if (fooI != fooEnd && barI != barEnd) {
					auto& I = (fooI->id < barI->id ? fooI : barI);
					if (fooI->id == barI->id) {
						mix.id = I->id;
						mix.pos = ::Lerp(fooI->pos, barI->pos, f);
						mix.chunks = I->chunks;
						mix.boostAmount = I->boostAmount;
						++fooI;
						++barI;
					}
					else {
						mix = *I++;
					}
				}
				else {
					if (fooI != fooEnd) {
						mix = *fooI++;
					}
					else if (barI != barEnd) {
						mix = *barI++;
					}
					else {
						break;
					}
				}
				lerped.spans.push_back(mix);
			}
		}
		return lerped;
	}

	FrameNumber frame;
	Board board;
	FrameState state[2];
	FrameState lerped;
};

bool Overlap(float aLeft, float aRight, float bLeft, float bRight) {
	return aRight >= bLeft && aLeft <= bRight;
}

float SpanSurface(Game::Span const& span) {
	return span.pos.y;
}

float2 SpanBegin(Game::Span const& span) {
	return span.pos;
}

float2 SpanEnd(Game::Span const& span) {
	return span.pos + (float)span.chunks * float2(SegmentLength, 0.0f);
}

bool SideHit(float2 position, Game::Span const& span) {
	enum { Left, Right };
	if (position.y > SpanSurface(span) - EjectDepth) {
		// NOTE(zao): Ship is not deep enough below surface
		return false;
	}

	float2 left = SpanBegin(span);
	float2 shipExtents[2] = {
		position - float2(PlayerWidth/2.0f, 0.0f),
		position + float2(PlayerWidth/2.0f, 0.0f),
	};

	if (shipExtents[Left].x <= left.x && shipExtents[Right].x >= left.x) {
		// NOTE(zao): Ship straddles span beginning
		return true;
	}
	return false;
}

bool SlideWorthy(float2 position, Game::Span const& span) {
	float left = span.pos.x;
	float right = left + (span.chunks-1) * SegmentLength;
	float top = span.pos.y;
	float bottom = span.pos.y - EjectDepth;
	float playerLeft = position.x - PlayerWidth;
	float playerRight = position.x + PlayerWidth;
	if (!Overlap(playerLeft, playerRight, left, right)) {
		return false;
	}
	if (!Overlap(position.y, position.y, bottom, top)) {
		return false;
	}
	return true;
}

float2 SpanLightPosition(Game::Span const& span, size_t index) {
	size_t n = span.chunks;
	float2 sOff = float2(0.0f, 10.0f);
	if (index) {
		sOff.x += (n-1) * SegmentLength;
	}
	float2 ret = span.pos + sOff;
	return ret;
}

float3 SpanLightColour(Game::Span const& span, size_t index) {
	float3 const ForwardColor = float3(0.3f, 1.0f, 0.3f);
	float3 const BackwardColor = float3(1.0f, 0.3f, 0.3f);
	return span.boostAmount > 0 ? ForwardColor : BackwardColor;
}

float SpanLightIntensity(Game::Span const& span, size_t index) {
	float mag = fabs(span.boostAmount);
	return Lerp(10.0f, 25.0f, mag / 50.0f);
};

bool MaybeSpawnSpan(Game& game) {
	auto& state = game.State();
	float2 lastEnd = SpanEnd(state.spans.back());

	if (lastEnd.x - state.bestPlayerPos > 300.0f) {
		return false;
	}

	auto& lcg = game.board.spanLCG;
	Game::Span span = {};
	span.id = game.board.nextSpanId++;
	float hGap = lcg.FloatIncl(20.0f, 40.0f);
	float vGap = lcg.FloatIncl(-20.0f, 20.0f);
	span.pos = lastEnd + float2(hGap, vGap);
	span.chunks = game.board.spanLCG.Int(5, 9);
	span.boostAmount = BoostAcceleration * game.board.spanLCG.FloatIncl(-2.0, 5.0f);
	state.spans.push_back(span);
	return true;
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	float2 const ViewSize = { 1280.0f, 720.0f };
	float const ViewAspect = ViewSize.x / ViewSize.y;
	GameWindow window;
	window.CreateCentered(ViewSize);
	bgfx::winSetHwnd(window);
	bgfx::init();
	Vertex::Init();
	CRect clientRect;
	window.GetClientRect(&clientRect);
	uint32_t const gfxFlags = BGFX_RESET_MSAA_X16 | BGFX_RESET_VSYNC;
	bgfx::reset(clientRect.Width(), clientRect.Height(), gfxFlags);
	uint32_t const gfxDebug = BGFX_DEBUG_TEXT;
	bgfx::setDebug(gfxDebug);
	window.ShowWindow(SW_NORMAL);

	bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x101018FF);

	slideLeft = Load7GM(L"data\\meshes\\slide-left.7gm");
	slideMid = Load7GM(L"data\\meshes\\slide-mid.7gm");
	slideRight = Load7GM(L"data\\meshes\\slide-right.7gm");
	ship = Load7GM(L"data\\meshes\\planey.7gm");

	bgfx::ProgramHandle singleProgram;
	{
		auto vsh = LoadShader("single_vs");
		auto fsh = LoadShader("single_fs");
		singleProgram = bgfx::createProgram(vsh, fsh, true);
	}
	bgfx::UniformHandle uLightPositions =
		bgfx::createUniform("u_worldLight", bgfx::UniformType::Uniform4fv, 16);
	bgfx::UniformHandle uLightColoursIntensities =
		bgfx::createUniform("u_lightColourIntensity", bgfx::UniformType::Uniform4fv, 16);
	bgfx::UniformHandle uLightCount =
		bgfx::createUniform("u_lightCount", bgfx::UniformType::Uniform1iv);

	Frustum frustum;
	frustum.SetKind(FrustumSpaceD3D, FrustumRightHanded);
	frustum.SetViewPlaneDistances(0.1f, 2000.0f);
	frustum.SetVerticalFovAndAspectRatio(pi/4.0f, ViewAspect);

	Game game;
	game.Reset();
	game.board.spanLCG.Seed(9001u);

	while (MaybeSpawnSpan(game)) {}

	auto const TicksPerSecond = bx::getHPFrequency();
	int64_t const TicksPerFrame = (int64_t)(TicksPerSecond / FramesPerSecond);
	int64_t const BaseTick = bx::getHPCounter();
	int64_t lastFrameTick = 0;

	while (1) {
		MSG msg = {};
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (msg.message == WM_QUIT) {
			break;
		}

		if (window.wantReset) {
			window.wantReset = false;
			game.Reset();
		}

		// NOTE(zao): Game logic
#if 1
		auto const NowTick = (bx::getHPCounter() - BaseTick);
#else
		auto const NowTick = 0ll;
#endif
		FrameNumber const StretchedFrameTime = (FrameNumber)(TicksPerFrame * TimeScale);
		while (lastFrameTick + StretchedFrameTime <= NowTick) {
			lastFrameTick += StretchedFrameTime;
			game.AdvanceFrame();
			auto& board = game.board;
			auto& state = game.State();
			float const dt = 1.0f / FramesPerSecond;

			while (MaybeSpawnSpan(game)) {}

			if (!state.dead) {
				if (state.jumping) {
					++state.jumpFramesDone;
					if (state.jumpFramesDone >= MaxJumpFrameCount || !window.wantJump && state.jumpFramesDone >= MinJumpFrameCount) {
						state.jumping = false;
						state.falling = true;
					}
				}
				bool const grounded = std::any_of(state.spans.begin(), state.spans.end(), [&](Game::Span const& span) {
					return SlideWorthy(state.playerPos, span);
				});
				if (grounded && window.wantJump) {
					state.jumping = true;
					state.falling = false;
					state.jumpFramesDone = 0;
				}

				state.playerVel = float2::zero;
				if (state.falling) {
					state.playerVel += float2(0.0f, FallVelocity) * dt;
				}
				if (state.jumping) {
					state.playerVel += float2(0.0f, JumpVelocity) * dt;
				}
				if (state.boosting) {
					state.playerSpeed += state.boostAmount * dt;
				}
				state.playerVel.x += state.playerSpeed;
				state.playerPos += state.playerVel * dt;
				state.bestPlayerPos = std::max(state.bestPlayerPos, state.playerPos.x);
				state.score += (int)(ScorePerSpeedTick * state.playerSpeed * dt);

				state.dead = false;
				state.boosting = false;
				for (auto span : state.spans) {
					if (SlideWorthy(state.playerPos, span)) {
						float maxEject = state.playerPos.y + EjectRate * dt;
						state.playerPos.y = std::min(SpanSurface(span), maxEject);
						state.playerVel.y = 0.0f;
						state.boosting = true;
						state.boostAmount = span.boostAmount;
					}
					if (SideHit(state.playerPos, span)) {
						state.dead = true;
						state.playerDeathFrame = game.frame;
					}
				}
			}
			else {
				state.playerVel *= 0.99f;
				state.playerPos += state.playerVel * dt;
			}
			state.cameraPos = Lerp(state.cameraPos, state.playerPos, CameraFollowFactor);
		}
		auto t = lastFrameTick / (float)TicksPerSecond;

		// NOTE(zao): View setup
		bgfx::setViewRect(0, 0, 0, clientRect.Width(), clientRect.Height());
		{
			float3 cpos = float3(game.State().cameraPos, 0.0f);
			float3 eyeBase(0.0f, 30.0f, 40.0f);
			float3 maxEyeShift(-70.0f, 0.0f, 700.0f);
			float3 eye = eyeBase + Lerp(float3::zero, maxEyeShift, Clamp01(game.State().playerSpeed / CameraDistanceFactor));
			float3 at = float3::zero;
			eye += cpos;
			at += cpos + float3(CameraLookAhead, 0.0f, 0.0f);
			float3x4 C = float3x4::LookAt(eye, at, -float3::unitZ, float3::unitY, float3::unitY);
			frustum.SetWorldMatrix(C);
		}

		bgfx::setViewTransform(0,
			float4x4(frustum.ViewMatrix()).Transposed().ptr(),
			float4x4(frustum.ProjectionMatrix()).Transposed().ptr());

		bgfx::submit(0);

		// NOTE(zao): Drawing
		float const F = Lerp(1.0f, 0.0f, (NowTick - lastFrameTick) / (float)StretchedFrameTime);
		auto renderState = game.ResolveState(F);
		struct Lights
		{
			float4 positions[16];
			float4 coloursIntensities[16];
			uint32_t count;
		};

		auto drawSimpleMesh = [&](Mesh mesh, float3x4 xform) {
			bgfx::setProgram(singleProgram);
			bgfx::setVertexBuffer(mesh.vbh);
			float4x4 world(xform);
			bgfx::setTransform(world.Transposed().ptr());
			bgfx::setState(BGFX_STATE_DEFAULT);
			bgfx::submit(0);
		};

		auto applyLights = [&](Lights const& lights) {
			bgfx::setUniform(uLightCount, &lights.count);
			bgfx::setUniform(uLightPositions, lights.positions, 16);
			bgfx::setUniform(uLightColoursIntensities, lights.coloursIntensities, 16);
		};

		struct LightInfo {
			float3 position;
			float3 colour;
			float intensity;
		};

		std::vector<LightInfo> forcedLights;
		{
			LightInfo li = {};
			float2 pos = renderState.playerPos;
			li.position = float3(pos, 0.0f) + float3(0.0f, 20.0f, 50.0f);
			li.colour = float3(1.0f, 1.0f, 1.0f);
			li.intensity = 150.0f;
			forcedLights.push_back(li);
		}
		std::vector<LightInfo> lightInfos;
		{
			for (auto& span : renderState.spans) {
				for (size_t i = 0; i < 2; ++i) {
					LightInfo li = {};
					li.position = float3(SpanLightPosition(span, i), 0.0f);
					li.colour = SpanLightColour(span, i);
					li.intensity = SpanLightIntensity(span, i);
					lightInfos.push_back(li);
				}
			}
		}

		auto const DetermineLightSet = [](std::vector<LightInfo> const& mandatory, std::vector<LightInfo> const& optional, float3 const pos) -> Lights {
			Lights lights = {};
			auto nMandatory = std::min<uint32_t>((uint32_t)mandatory.size(), 16u);
			for (size_t i = 0; i < nMandatory; ++i, ++lights.count) {
				auto& sourceLight = mandatory[i];
				lights.positions[lights.count] = float4(sourceLight.position, 0.0f);
				lights.coloursIntensities[lights.count] = float4(sourceLight.colour, sourceLight.intensity);
			}
			size_t nOptional = std::min<size_t>(optional.size(), 16u - lights.count);
			std::multimap<float, LightInfo const*> ranking;
			for (auto& l : optional) {
				float distSq = l.position.DistanceSq(pos);
				if (ranking.size() < nOptional) {
					ranking.emplace(distSq, &l);
				}
				else {
					auto last = --ranking.end();
					if (distSq < last->first) {
						ranking.erase(last);
						ranking.emplace(distSq, &l);
					}
				}
			}
			auto rankI = ranking.begin();
			for (size_t i = 0; i < nOptional; ++i, ++rankI, ++lights.count) {
				auto& sourceLight = *rankI->second;
				lights.positions[lights.count] = float4(sourceLight.position, 0.0f);
				lights.coloursIntensities[lights.count] = float4(sourceLight.colour, sourceLight.intensity);
			}
			return lights;
		};

		auto drawTrackSegment = [&](Game::Span span) {
			float2 pos = span.pos;
			int chunks = (int)span.chunks;
			float const Width = chunks * SegmentLength;
			float3 midpoint = float3(pos + float2(Width/2.0f, 0.0f), 0.0f);
			auto lights = DetermineLightSet(forcedLights, lightInfos, midpoint);
			applyLights(lights);
			drawSimpleMesh(slideLeft, float3x4::Translate(pos.x - 10.0f, pos.y, 0.0f));
			for (int i = 0; i < chunks; ++i) {
				float3x4 T = float3x4::Translate(pos.x + i*SegmentLength, pos.y, 0.0f);
				drawSimpleMesh(slideMid, T);
			}
			drawSimpleMesh(slideRight, float3x4::Translate(pos.x + Width, pos.y, 0.0f));
		};
		for (auto& span : renderState.spans) {
			drawTrackSegment(span);
		}

		{
			float3 shipRenderPos(renderState.playerPos, 0.0f);
			auto lights = DetermineLightSet(forcedLights, lightInfos, shipRenderPos);
			applyLights(lights);
			float3x4 T = float3x4::Translate(shipRenderPos);
			float3x3 S = float3x3::identity;
			if (renderState.dead) {
				float const DeathAnimationDuration = 0.3f;
				float timeSinceDeath = Clamp((float)(game.frame + F - renderState.playerDeathFrame) / FramesPerSecond, 0.0f, DeathAnimationDuration);
				float scale = Lerp(1.0f, 0.0f, timeSinceDeath / DeathAnimationDuration);
				S = float3x3::UniformScale(scale);
			}
			drawSimpleMesh(ship, T*S);
		}

		bgfx::dbgTextClear(0);
		bgfx::dbgTextPrintf(100, 50, 0x3F, "Score: %ld", renderState.score);
		bgfx::dbgTextPrintf(100, 52, 0x2F, "Speed: %.2f", renderState.playerSpeed);

		bgfx::frame();
	}
	if (window) { window.DestroyWindow(); }
	bgfx::destroyUniform(uLightPositions);
	bgfx::destroyUniform(uLightColoursIntensities);
	bgfx::destroyUniform(uLightCount);
	bgfx::destroyProgram(singleProgram);
	DestroyMesh(slideLeft);
	DestroyMesh(slideMid);
	DestroyMesh(slideRight);
	DestroyMesh(ship);
	bgfx::shutdown();
	return 0;
}
#!/usr/bin/env bash

bouncedir="$HOME/bounce"

function clone () {
	if [ ! -d "$bouncedir/$1" ]; then
		cd $bouncedir
		git clone "$2" || exit 1
	fi
}

if [ ! -d "$bouncedir" ]; then mkdir -p "$bouncedir"; fi
clone bx https://github.com/bkaradzic/bx.git
clone bgfx https://github.com/bkaradzic/bgfx.git
clone MathGeoLib https://github.com/juj/MathGeoLib.git

cd "$bouncedir/bgfx" || exit
make && cd .build/projects/gmake-linux && make config=release64 -j2

cd "$bouncedir/MathGeoLib" || exit
mkdir include
ln -s ../src include/MathGeoLib
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release && make -j2

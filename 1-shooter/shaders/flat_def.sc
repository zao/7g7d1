vec4 v_color0    : COLOR0    = vec4(1.0, 0.0, 0.0, 1.0);

vec3 a_position  : POSITION;
vec4 a_color0    : COLOR0;

vec4 i_w0     : TEXCOORD5;
vec4 i_w1     : TEXCOORD6;
vec4 i_w2     : TEXCOORD7;
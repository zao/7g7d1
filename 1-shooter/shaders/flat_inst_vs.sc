$input a_position, a_color0, i_w0, i_w1, i_w2
$output v_color0

#include <bgfx_shader.sh>

void main()
{
	vec4 in_pos = vec4(a_position, 1.0);
	vec4 pos = {
		dot(i_w0, in_pos),
		dot(i_w1, in_pos),
		dot(i_w2, in_pos),
		1.0
	};
	gl_Position = mul(u_modelViewProj, pos);
	v_color0 = a_color0;
}

@mkdir data\shaders\dx11 2>NUL
shaderc -I shaders --platform windows --varyingdef shaders\flat_def.sc --profile vs_5_0 --type vertex -f shaders\flat_inst_vs.sc -o data\shaders\dx11\flat_inst_vs.bin
shaderc -I shaders --platform windows --varyingdef shaders\flat_def.sc --profile vs_5_0 --type vertex -f shaders\flat_vs.sc -o data\shaders\dx11\flat_vs.bin
shaderc -I shaders --platform windows --varyingdef shaders\flat_def.sc --profile ps_5_0 --type frag   -f shaders\flat_fs.sc -o data\shaders\dx11\flat_fs.bin
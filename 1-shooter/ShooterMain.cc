#include "Shape.h"

#include <Windows.h>
#include <atlbase.h>
#include <atlapp.h>
#include <atltypes.h>
#include <atlctl.h>
#include <atlcrack.h>
#include <atlframe.h>
#include <atlwin.h>
#include <bx/timer.h>
#include <bgfx.h>
#include <bgfxplatform.h>
#include <MathGeoLib/MathGeoLib.h>

#include <algorithm>
#include <deque>
#include <list>
#include <sstream>

namespace WTL {
	using ::CPoint;
}

uint32_t ABGR(uint8_t a, uint8_t b, uint8_t g, uint8_t r) {
	uint32_t ret = (a << 24) | (b << 16) | (g << 8) | r;
	return ret;
}

uint32_t ABGR(float a, float b, float g, float r) {
	return ABGR((uint8_t)(a * 255.0f), (uint8_t)(b * 255.0f), (uint8_t)(g * 255.0f), (uint8_t)(r * 255.0f));
}

struct GameWindow : CFrameWindowImpl<GameWindow>
{
	BEGIN_MSG_MAP(GameWindow)
		MSG_WM_DESTROY(OnDestroy)
		MSG_WM_SETCURSOR(OnSetCursor)
		MSG_WM_KEYUP(OnKeyUp)
		MSG_WM_LBUTTONDOWN(OnLMBDown)
	END_MSG_MAP()

	void OnDestroy() {
		PostQuitMessage(0);
	}

	BOOL OnSetCursor(CWindow wnd, UINT hitTest, UINT message) {
		if (hitTest == HTCLIENT) {
			SetCursor(NULL);
			return TRUE;
		}
		DefWindowProc();
		return FALSE;
	}

	void OnKeyUp(UINT ch, UINT repCount, UINT flags) {
		if (ch == VK_ESCAPE) {
			DestroyWindow();
		}
	}

	void OnLMBDown(UINT flags, CPoint p) {
		clicks.push_back(p);
	}

	float2 ClientToWorld(float2 client, float2 world) {
		CRect cr;
		GetClientRect(&cr);
		float cw = (float)cr.Width();
		float ch = (float)cr.Height();
		float2 ret = {
			Lerp(0.5f, world.x - 0.5f, client.x / (cw - 1.0f)),
			Lerp(world.y - 0.5f, 0.5f, client.y / (ch - 1.0f))
		};
		return ret;
	}

	std::deque<CPoint> clicks;
};

struct SlurpResult {
	std::vector<char> data;

	operator bgfx::Memory const* () const {
		uint32_t n = (uint32_t)data.size();
		assert(n == data.size());
		return bgfx::copy(data.data(), n);
	}
};

std::string RendererName() {
	switch (bgfx::getRendererType()) {
	case bgfx::RendererType::Direct3D9: return "dx9";
	case bgfx::RendererType::Direct3D11:
	case bgfx::RendererType::Direct3D12: return "dx11";
	case bgfx::RendererType::OpenGL: return "glsl";
	case bgfx::RendererType::OpenGLES: return "gles";
	default: abort();
	}
}

SlurpResult SlurpAsset(std::string filepath) {
	HANDLE h = CreateFileA(filepath.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_DELETE, 0, OPEN_ALWAYS, 0, 0);
	if (!h) {
		return SlurpResult{};
	}
	LARGE_INTEGER cb = {};
	GetFileSizeEx(h, &cb);
	if (cb.HighPart) {
		CloseHandle(h);
		return SlurpResult{};
	}
	DWORD n = cb.LowPart;
	SlurpResult ret;
	ret.data.resize(n);
	DWORD nRead = 0;
	BOOL res = ReadFile(h, ret.data.data(), n, &nRead, 0);
	CloseHandle(h);
	if (!res || nRead != n) {
		return SlurpResult{};
	}
	return ret;
}

bgfx::ShaderHandle LoadShader(std::string name) {
	std::string renderer = RendererName();
	std::string filepath = "data\\shaders\\" + renderer + "\\" + name + ".bin";
	auto bin = SlurpAsset(filepath);
	return bgfx::createShader(bin);
}

bgfx::VertexDecl FlatShapeVertex::decl;

static Shape
	cityShape, ruinShape,
	cannonMountShape, cannonPipeShape,
	bulletShape,
	missileShape, missilePuffShape,
	groundShape,
	crosshairShape,
	explosionShape,
	landingShape;

typedef int64_t FrameNumber;

static float const framesPerSecond = 720.0f;
static float const BulletVelocity = 600.0f;
static float const MissileVelocity = 100.0f;
static float const ExplosionRadius = 75.0f;
static float const LandingRadius = 20.0f;
static float const PuffSpin = 3.0f;
static FrameNumber const ExplosionDuration = (FrameNumber)(framesPerSecond * 0.3f);
static FrameNumber const LandingDuration = (FrameNumber)(framesPerSecond * 0.5f);
static FrameNumber const MissileIntervalLow = (FrameNumber)(framesPerSecond * 0.3f);
static FrameNumber const MissileIntervalHigh = (FrameNumber)(framesPerSecond * 0.6f);
static FrameNumber const MissilePuffDuration = (FrameNumber)(framesPerSecond * 0.5f);
static FrameNumber const MissilePuffInterval = (FrameNumber)(framesPerSecond * 0.03f);

void SetupShapes() {
	{
		uint32_t groundColour = 0xFF335533;
		std::vector<float2> heights;
		size_t heightCount = 100;
		for (size_t i = 0; i < heightCount; ++i) {
			float x = 1280.0f * i / (float)(heightCount - 1);
			float y = 25.0f + 18.0f * (rand() / (float)RAND_MAX);
			heights.push_back({ x, y });
		}
		ShapeBuilder sb;
		for (size_t i = 0; i < heightCount - 1; ++i) {
			sb.AddTrapezoid(heights[i].x, heights[i+1].x, 0.0f,
				heights[i].y, heights[i+1].y, 0.0f, groundColour);
		}
		groundShape = sb.Create();
	}
	{
		ShapeBuilder sb;
		sb.AddRect({ -5.0f, -3.0f}, { 5.0f, 20.0f }, 0.0f, 0xFF777777);
		sb.AddRect({ -3.0f, -3.0f }, { 4.0f, 25.0f }, 0.0f, 0xFF774444);
		sb.AddRect({ 3.0f, -4.0f }, { 8.0f, 32.0f }, 0.0f, 0xFF447744);
		cityShape = sb.Create();
	}
	{
		ShapeBuilder sb;
		sb.AddRect({ -5.0f, -3.0f }, { 5.0f, 5.0f }, 0.0f, 0xFF777777);
		sb.AddRect({ -3.0f, -3.0f }, { 4.0f, 4.0f }, 0.0f, 0xFF774444);
		sb.AddRect({ 3.0f, -4.0f }, { 8.0f, 8.0f }, 0.0f, 0xFF447744);
		ruinShape = sb.Create();
	}
	{
		ShapeBuilder sb;
		for (int i = 0; i < 4; ++i) {
			float ang = Lerp(0.0f, 2*pi, i / 4.0f);
			float a0 = ang - 0.3f, a1 = ang + 0.3f;
			float2 p0 = float2(cos(a0), sin(a0));
			float2 p1 = float2(cos(a1), sin(a1));
			float3 ps[] = {
				float3(15.0f * p0, 0.0f),
				float3(15.0f * p1, 0.0f),
				float3(3.0f * p1, 0.0f),
				float3(3.0f * p0, 0.0f),
			};
			sb.AddQuad(ps[0], ps[1], ps[2], ps[3], 0xFFDDDDDD);
		}
		crosshairShape = sb.Create();
	}
	{
		float w = 30.0f;
		float h = 20.0f;
		ShapeBuilder sb;
		float3 arcVs[12] = {};
		for (size_t i = 0; i < 12; ++i) {
			float const ang = Lerp(0.0f, pi, i / 11.0f);
			arcVs[i] = float3(w/2.0f*cos(ang), w/2.0f * sin(ang), 0.0f);
		}
		for (size_t i = 0; i < 11; ++i) {
			sb.AddTriangle(float3::zero, arcVs[i], arcVs[i+1], ABGR(1.0f, 0.7f, 0.7f, 0.8f));
		}
		float3 bLeft = { -w/2.0f, -h, 0.0f };
		float3 bRight = { w/2.0f, -h, 0.0f };
		sb.AddQuad(bLeft, bRight, arcVs[0], arcVs[11], ABGR(1.0f, 0.7f, 0.7f, 0.8f));
		cannonMountShape = sb.Create();
	}
	{
		float hw = 14.0f / 2.0f;
		float h = 30.0f;
		float lipW = 3.0f;
		float lipH = 3.0f;
		ShapeBuilder sb;
		sb.AddRect(float2(-hw, 0.0f), float2(hw, h), -1.0f, 0xFF447755);
		sb.AddRect(float2(-hw - lipW, h-lipH), float2(hw + lipW, h + lipH), -0.9f, 0xFF558866);
		cannonPipeShape = sb.Create();
	}
	{
		float w = 6.0f;
		float h = 9.0f;
		float ex = 5.0f;
		float exW = w / 2.0f;
		ShapeBuilder sb;
		sb.AddTriangle(
			float3(-w/2.0f, 0.0f, 0.0f),
			float3(w/2.0f, 0.0f, 0.0f),
			float3(0.0f, h, 0.0f),
			0xFFAAAAAA);
		sb.AddTriangle(
			float3(-exW/2.0f, 0.0f, 0.0f),
			float3(0.0f, -ex, 0.0),
			float3(exW/2.0f, 0.0f, 0.0f),
			0xFF116090);
		missileShape = sb.Create();
	}
	{
		ShapeBuilder sb;
		float const r = 5.0f;
		float3 mid = float3::zero;
		float3 vs[8];
		for (unsigned i = 0; i < 8; ++i) {
			float ang = Lerp(0.0f, 2*pi, (float)i/8.0f);
			vs[i] = { r * cos(ang), r * sin(ang), 0.0f };
		}
		for (unsigned i = 0; i < 8; ++i) {
			sb.AddTriangle(mid, vs[i], vs[(i+1)%8], (i&1) ? ABGR(1.0f, 0.3f, 0.3f, 0.3f) : ABGR(1.0f, 0.5f, 0.5f, 0.5f));
		}
		bulletShape = sb.Create();
	}
	{
		ShapeBuilder sb;
		float const r = ExplosionRadius;
		float3 mid = float3::zero;
		size_t const CornerCount = 24;
		float3 vs[CornerCount];
		uint32_t cs[2] = { ABGR(1.0f, 0.0f, 0.9f, 0.8f), ABGR(1.0f, 0.0f, 0.8f, 0.9f) };
		for (size_t i = 0; i < CornerCount; ++i) {
			float ang = Lerp(0.0f, 2*pi, (float)i/CornerCount);
			vs[i] = { r * cos(ang), r * sin(ang), 0.0f };
		}
		for (size_t i = 0; i < CornerCount; ++i) {
			sb.AddTriangle(mid, vs[i], vs[(i+1)%CornerCount], cs[i%2]);
		}
		explosionShape = sb.Create();
	}
	{
		ShapeBuilder sb;
		float const HalfBaseWidth = 10.0f;
		float const HalfSpread = 1.0f;
		size_t const JabCount = 5;
		float jabs[JabCount] = { 20.0f, 10.0f, 30.0f, 22.0f, 15.0f };
		float3 ups[JabCount];
		uint32_t cs[2] = { ABGR(1.0f, 0.0f, 0.5f, 0.8f), ABGR(1.0f, 0.0f, 0.5f, 0.9f) };
		for (unsigned i = 0; i < JabCount; ++i) {
			ups[i] = float3x3::RotateZ(Lerp(HalfSpread, -HalfSpread, (float)i / (JabCount - 1))).Mul(float3(0.0f, jabs[i], 0.0f));
		}
		for (unsigned i = 0; i < JabCount - 1; ++i) {
			float3 baseL = { Lerp(-HalfBaseWidth, HalfBaseWidth, (float)i / (JabCount-1)), 0.0f, 0.0f };
			float3 baseR = { Lerp(-HalfBaseWidth, HalfBaseWidth, (float)(i+1) / (JabCount-1)), 0.0f, 0.0f };
			sb.AddQuad(baseL, baseR, baseR + ups[i+1], baseL + ups[i], cs[i%2]);
		}
		landingShape = sb.Create();
	}
	{
		ShapeBuilder sb;
		float3 vs[] = {
			3.0f * float3::unitX,
			3.0f * float3::unitY,
			3.0f * -float3::unitX,
			3.0f * -float3::unitY,
		};
		sb.AddQuad(vs[0], vs[1], vs[2], vs[3], ABGR(1.0f, 0.3f, 0.3f, 0.3f));
		missilePuffShape = sb.Create();
	}
}

struct City {
	float2 pos;
	bool dead;
	FrameNumber deadSince;
};

struct Bullet {
	float2 pos;
	float2 vel;
	float2 aim;
};

struct Missile {
	float2 pos;
	float2 vel;
	bool isHit;
	FrameNumber lastPuff;
};

struct Explosion {
	float2 pos;
	FrameNumber spawnFrame;
	FrameNumber duration;
};

struct Puff {
	float2 pos;
	float2 vel;
	float angle;
	float aVel;
	FrameNumber spawnFrame;
	FrameNumber duration;
};

struct Game {
	std::vector<City> cities;
	std::vector<Bullet> bullets;
	std::vector<Missile> missiles;
	std::vector<Explosion> explosions;
	std::vector<Explosion> landings;
	std::vector<Puff> puffs;

	float2 playerPos;
	float2 targetPos;

	FrameNumber frame;
	FrameNumber nextShotAllowed;

	FrameNumber nextMissileSpawn;
	LCG missileSpawnLCG;
	LCG puffLCG;

	uint32_t score;
	bool gameOver;
};

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	GameWindow gameWindow;
	{
		HWND wnd = gameWindow.Create(0);
		bgfx::winSetHwnd(wnd);
	}
	bgfx::init();
	uint32_t clientWidth = 1, clientHeight = 1;
	{
		CRect dims;
		GetClientRect(gameWindow, &dims);
		clientWidth = dims.Width();
		clientHeight = dims.Height();
	}
	uint32_t const resetFlags = BGFX_RESET_VSYNC | BGFX_RESET_MSAA_X16;
	uint32_t const debugFlags = BGFX_DEBUG_TEXT;
	bgfx::reset(clientWidth, clientHeight, resetFlags);
	bgfx::setDebug(debugFlags);
	ShowWindow(gameWindow, SW_NORMAL);

	FlatShapeVertex::Init();
	SetupShapes();

	FlatShapeVertex sampleVertices[] = {
		// 0xAABBGGRR
		{ { 0.0f, 0.0f, 0.0f }, 0xFF0000FF },
		{ { 16.0f, 0.0f, 0.0f }, 0xFF00FF00 },
		{ { 0.0f, 8.0f, 0.0f }, 0xFFFF0000 },
	};

	uint16_t sampleIndices[] = {
		0, 1, 2,
	};

	bgfx::VertexBufferHandle vbh = bgfx::createVertexBuffer(
		bgfx::makeRef(sampleVertices, sizeof(sampleVertices)),
		FlatShapeVertex::decl);

	bgfx::IndexBufferHandle ibh = bgfx::createIndexBuffer(
		bgfx::makeRef(sampleIndices, sizeof(sampleIndices)));

	bgfx::ShaderHandle instanceVsh = LoadShader("flat_inst_vs");
	bgfx::ShaderHandle instanceFsh = LoadShader("flat_fs");
	bgfx::ShaderHandle vsh = LoadShader("flat_vs");
	bgfx::ShaderHandle fsh = LoadShader("flat_fs");
	bgfx::ProgramHandle instancedProgram = bgfx::createProgram(instanceVsh, instanceFsh, true);
	bgfx::ProgramHandle program = bgfx::createProgram(vsh, fsh, true);

	bgfx::setViewClear(0,
		BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH,
		0x102030ff,
		1.0f,
		0);

	Game game = {};
	for (float i = 0.0f; i <= 5.0f; i += 1.0f) {
		City c = {};
		float const y = 20.0f;
		c.pos = Lerp(float2(100.0f, y), float2(1180.0f, y), i/5.0f);
		game.cities.push_back(c);
	}
	game.playerPos = float2(1280.0f / 2.0f, 30.0f);

	int64_t timeOffset = bx::getHPCounter();
	int64_t timeFrequency = bx::getHPFrequency();
	int64_t countsPerFrame = (int64_t)(timeFrequency / framesPerSecond);
	int64_t last = timeOffset;
	while (1) {
		MSG msg = {};
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (msg.message == WM_QUIT) {
			break;
		}

		int64_t now = bx::getHPCounter();
		int64_t const frameTime = now - last;

		CPoint cursorPoint;
		BOOL volatile succ = GetCursorPos(&cursorPoint);
		BOOL volatile succ2 = ScreenToClient(gameWindow, &cursorPoint);
		float2 cursorPos = gameWindow.ClientToWorld(float2((float)cursorPoint.x, (float)cursorPoint.y), float2(1280.0f, 720.0f));

		// Game logic
		while (last + countsPerFrame <= now) {
			float const dt = 1.0f / framesPerSecond;
			game.targetPos = cursorPos;
			for (auto& puff : game.puffs) {
				puff.pos += puff.vel * dt;
				puff.angle += puff.aVel * dt;
			}
			for (auto& bullet : game.bullets) {
				bullet.pos += bullet.vel * dt;
			}
			for (auto& missile : game.missiles) {
				missile.pos += missile.vel * dt;
			}
			for (auto& city : game.cities) {
			}
			for (auto& missile : game.missiles)
			{
				if (missile.lastPuff + MissilePuffInterval == game.frame) {
					Puff p = {};
					p.pos = missile.pos;
					p.vel = -missile.vel * 0.3f;
					p.angle = game.puffLCG.FloatIncl(0.0f, 2.0f * pi);
					p.aVel = game.puffLCG.Int(0, 1) ? -PuffSpin : PuffSpin;
					p.spawnFrame = game.frame;
					p.duration = MissilePuffDuration;
					game.puffs.push_back(p);
					missile.lastPuff = game.frame;
				}
			}
			{
				auto& c = game.puffs;
				c.erase(std::remove_if(c.begin(), c.end(), [&](Puff const& puff)
				{
					return puff.spawnFrame + puff.duration == game.frame;
				}), c.end());
			}
			{
				auto& c = game.bullets;
				auto dueI = std::partition(c.begin(), c.end(), [&](Bullet const& bullet)
				{
					return (bullet.pos - game.playerPos).LengthSq() < (bullet.aim - game.playerPos).LengthSq();
				});
				std::for_each(dueI, c.end(), [&](Bullet const& b)
				{
					Explosion e = {};
					e.pos = b.aim;
					e.spawnFrame = game.frame;
					e.duration = ExplosionDuration;
					game.explosions.push_back(e);

					for (auto& m : game.missiles) {
						if (m.pos.DistanceSq(e.pos) < ExplosionRadius*ExplosionRadius) {
							m.isHit = true;
							game.score += 200;
						}
					}
				});
				c.erase(dueI, c.end());
			}

			for (auto& m : game.missiles) {
				if (m.pos.y < 20.0f) {
					m.isHit = true;
					Explosion e = {};
					e.pos = m.pos;
					e.spawnFrame = game.frame;
					e.duration = LandingDuration;
					game.landings.push_back(e);
					for (auto& city : game.cities) {
						if (city.dead) continue;
						float const d = fabs(city.pos.x - m.pos.x);
						if (d < 20.0f) {
							city.dead = true;
							city.deadSince = game.frame;
						}
					}
				}
			}
			auto ruinCount = std::count_if(game.cities.begin(), game.cities.end(), [](City const& c) { return c.dead; });
			if (ruinCount == game.cities.size())
				game.gameOver = true;

			{
				auto& c = game.missiles;
				auto dueI = std::remove_if(c.begin(), c.end(), [&](Missile const& missile)
				{
					return missile.pos.y < -5.0f || missile.pos.x < -5.0f || missile.pos.x > 1280.0f + 5.0f || missile.isHit;
				});
				c.erase(dueI, c.end());
			}
			{
				auto shouldErase = [&](Explosion const& explosion) -> bool
				{
					return explosion.spawnFrame + explosion.duration == game.frame;
				};
				{
					auto& c = game.explosions;
					c.erase(std::remove_if(c.begin(), c.end(), shouldErase), c.end());
				}
				{
					auto& c = game.landings;
					c.erase(std::remove_if(c.begin(), c.end(), shouldErase), c.end());
				}
			}

			if (!game.gameOver) {
				for (auto click : gameWindow.clicks) {
					Bullet bullet;
					float2 aim = gameWindow.ClientToWorld(float2((float)click.x, (float)click.y), float2(1280.0f, 720.0f));
					float2 dir = (aim - game.playerPos).Normalized();
					bullet.pos = game.playerPos + dir * 20.0f;
					bullet.aim = aim;
					bullet.vel = dir * BulletVelocity;
					game.bullets.push_back(bullet);
				}
			}
			gameWindow.clicks.clear();

			if (game.frame >= game.nextMissileSpawn) {
				auto& lcg = game.missileSpawnLCG;
				Missile m = {};
				m.pos.x = lcg.FloatIncl(200.0f, 1280.0f - 200.0f);
				m.pos.y = 720.0f + 10.0f;
				float3x3 R = float3x3::RotateZ(lcg.FloatIncl(1.5f * pi - 0.4f, 1.5f * pi + 0.4f));
				float2 dir = R.MulDir(float3::unitX).xy();
				m.vel = dir * MissileVelocity;
				m.lastPuff = game.frame;
				game.missiles.push_back(m);
				game.nextMissileSpawn = game.frame + (FrameNumber)lcg.Int((int)MissileIntervalLow, (int)MissileIntervalHigh);
			}

			++game.frame;
			last += countsPerFrame;
		}

		// Drawing

		Frustum frustum;
		frustum.SetOrthographic(1280, 720);
		frustum.SetKind(FrustumSpaceD3D, FrustumRightHanded);
		frustum.SetFrame(float3(640.0f, 360.0f, 0.0f), -float3::unitZ, float3::unitY);
		frustum.SetViewPlaneDistances(0.0f, 1000.0f);
		float4x4 viewMatrix = frustum.ViewMatrix();
		float4x4 projMatrix = frustum.ProjectionMatrix();
		bgfx::setViewTransform(0, viewMatrix.Transposed().ptr(), projMatrix.Transposed().ptr());
		bgfx::setViewRect(0, 0, 0, clientWidth, clientHeight);
		bgfx::submit(0);

		bgfx::dbgTextClear();

		{
			{
				float const groundDepth = -20.0f;
				bgfx::setProgram(program);
				float4x4 world = float4x4::Translate(0.0f, 0.0f, groundDepth);
				bgfx::setTransform(world.Transposed().ptr());
				bgfx::setVertexBuffer(groundShape.vbh);
				bgfx::setIndexBuffer(groundShape.ibh);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}

			for (auto& city : game.cities) {
				float const cityDepth = -5.0f;
				bgfx::setProgram(program);
				float4x4 world = float4x4::Translate(float3(city.pos, cityDepth));
				bgfx::setTransform(world.Transposed().ptr());
				auto& shape = city.dead ? ruinShape : cityShape;
				bgfx::setVertexBuffer(shape.vbh);
				bgfx::setIndexBuffer(shape.ibh);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}

			{
				float const crosshairDepth = -0.1f;
				bgfx::setProgram(program);
				float4x4 world = float4x4::Translate(float3(game.targetPos, crosshairDepth));
				bgfx::setTransform(world.Transposed().ptr());
				bgfx::setVertexBuffer(crosshairShape.vbh);
				bgfx::setIndexBuffer(crosshairShape.ibh);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}
			{
				float const cannonDepth = -1.0f;
				bgfx::setProgram(program);
				float4x4 world = float4x4::Translate(float3(game.playerPos, cannonDepth));
				bgfx::setTransform(world.Transposed().ptr());
				bgfx::setVertexBuffer(cannonMountShape.vbh);
				bgfx::setIndexBuffer(cannonMountShape.ibh);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);

				bgfx::setProgram(program);
				float3 aim = float3((game.targetPos - game.playerPos).Normalized(), 0.0f);
				float4x4 cannonAim = float4x4::LookAt(float3::unitY, aim, float3::unitZ, float3::unitZ);
				bgfx::setTransform((world * cannonAim).Transposed().ptr());
				bgfx::setVertexBuffer(cannonPipeShape.vbh);
				bgfx::setIndexBuffer(cannonPipeShape.ibh);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}
			if (game.missiles.size()) {
				float const missileDepth = -2.0f;
				bgfx::setProgram(instancedProgram);
				uint32_t n = (uint32_t)game.missiles.size();
				auto idb = bgfx::allocInstanceDataBuffer(n, (uint16_t)sizeof(float3x4));
				{
					float3x4* data = (float3x4*)idb->data;
					for (auto missile : game.missiles) {
						float3x4 T = float3x4::Translate(float3(missile.pos, missileDepth));
						float3x3 R = float3x3::LookAt(float3::unitY, float3(missile.vel, 0.0f), float3::unitZ, float3::unitZ);
						float3x4 xform = T*R;
						memcpy(data, xform.ptr(), sizeof(float3x4));
						++data;
					}
				}
				bgfx::setTransform(float4x4::identity.Transposed().ptr());
				bgfx::setVertexBuffer(missileShape.vbh);
				bgfx::setIndexBuffer(missileShape.ibh);
				bgfx::setInstanceDataBuffer(idb, n);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}
			if (game.explosions.size()) {
				float const explosionDepth = -0.5f;
				bgfx::setProgram(instancedProgram);
				uint32_t n = (uint32_t)game.explosions.size();
				auto idb = bgfx::allocInstanceDataBuffer(n, (uint16_t)sizeof(float3x4));
				{
					float3x4* data = (float3x4*)idb->data;
					for (auto explosion : game.explosions) {
						float3x4 T = float3x4::Translate(float3(explosion.pos, explosionDepth));
						float3x3 R = float3x3::identity;
						float3x4 xform = T*R;
						memcpy(data, xform.ptr(), sizeof(float3x4));
						++data;
					}
				}
				bgfx::setTransform(float4x4::identity.Transposed().ptr());
				bgfx::setVertexBuffer(explosionShape.vbh);
				bgfx::setIndexBuffer(explosionShape.ibh);
				bgfx::setInstanceDataBuffer(idb, n);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}
			if (game.landings.size()) {
				float const landingDepth = -1.1f;
				bgfx::setProgram(instancedProgram);
				uint32_t n = (uint32_t)game.landings.size();
				auto idb = bgfx::allocInstanceDataBuffer(n, (uint16_t)sizeof(float3x4));
				{
					float3x4* data = (float3x4*)idb->data;
					for (auto landing : game.landings) {
						float3x4 T = float3x4::Translate(float3(landing.pos, landingDepth));
						float3x3 R = float3x3::identity;
						float3x4 xform = T*R;
						memcpy(data, xform.ptr(), sizeof(float3x4));
						++data;
					}
				}
				bgfx::setTransform(float4x4::identity.Transposed().ptr());
				bgfx::setVertexBuffer(landingShape.vbh);
				bgfx::setIndexBuffer(landingShape.ibh);
				bgfx::setInstanceDataBuffer(idb, n);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}
			if (game.puffs.size()) {
				float const puffDepth = -5.0f;
				bgfx::setProgram(instancedProgram);
				uint32_t n = (uint32_t)game.puffs.size();
				auto idb = bgfx::allocInstanceDataBuffer(n, (uint16_t)sizeof(float3x4));
				{
					float3x4* data = (float3x4*)idb->data;
					for (auto puff : game.puffs) {
						float3x4 T = float3x4::Translate(float3(puff.pos, puffDepth));
						float3x3 R = float3x3::RotateZ(puff.angle);
						float lifePercentage = (float)(game.frame - puff.spawnFrame) / puff.duration;
						float s = Lerp(1.0f, 3.0f, lifePercentage);
						if (lifePercentage > 1.0) s = 0.0f;
						float3x3 S = float3x3::UniformScale(s);
						float3x4 xform = T*R*S;
						memcpy(data, xform.ptr(), sizeof(float3x4));
						++data;
					}
				}
				bgfx::setTransform(float4x4::identity.Transposed().ptr());
				bgfx::setVertexBuffer(missilePuffShape.vbh);
				bgfx::setIndexBuffer(missilePuffShape.ibh);
				bgfx::setInstanceDataBuffer(idb, n);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}
			for (auto bullet : game.bullets) {
				float const bulletDepth = -10.0f;
				bgfx::setProgram(program);
				float4x4 T = float4x4::Translate(float3(bullet.pos, bulletDepth));
				float4x4 R = float4x4::LookAt(float3::unitY, float3(bullet.vel, 0.0f), float3::unitZ, float3::unitZ);
				bgfx::setTransform((T*R).Transposed().ptr());
				bgfx::setVertexBuffer(bulletShape.vbh);
				bgfx::setIndexBuffer(bulletShape.ibh);
				bgfx::setState(BGFX_STATE_DEFAULT);
				bgfx::submit(0);
			}
		}

		bgfx::dbgTextPrintf(200, 1, 0x0F, "Score: %07u", game.score);
		if (game.gameOver) {
			bgfx::dbgTextPrintf(120, 25, 0x3F, "GAME OVER");
		}

		bgfx::frame();
	}

	bgfx::destroyIndexBuffer(ibh);
	bgfx::destroyVertexBuffer(vbh);
	bgfx::destroyProgram(program);

	bgfx::shutdown();
	return 0;
}
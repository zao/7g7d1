#pragma once

#include <bgfx.h>
#include <MathGeoLib/MathGeoLib.h>
#include <vector>
#include <stdint.h>

struct FlatShapeVertex {
	float3 position;
	uint32_t color; // ABGR

	static void Init() {
		decl
			.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.end();
	}

	static bgfx::VertexDecl decl;
};

template <typename E>
bgfx::Memory const* Copy(std::vector<E> const& v) {
	auto cb = (uint32_t)(v.size() * sizeof(E));
	return bgfx::copy(v.data(), cb);
}

struct Shape {
	bgfx::VertexBufferHandle vbh;
	bgfx::IndexBufferHandle ibh;
};

struct ShapeBuilder {
	std::vector<FlatShapeVertex> vertices;
	std::vector<uint16_t> indices;

	// CCW order
	void AddQuad(float3 a, float3 b, float3 c, float3 d, uint32_t colour) {
		// a d   0 3
		// b c   1 2
		FlatShapeVertex corners[4] = {
			{ a, colour },
			{ b, colour },
			{ c, colour },
			{ d, colour },
		};
		uint16_t baseIdx = (uint16_t)vertices.size();
		vertices.insert(vertices.end(), corners, corners + 4);
		uint16_t nodes[6] = {
			0 + baseIdx, 1 + baseIdx, 3 + baseIdx,
			3 + baseIdx, 1 + baseIdx, 2 + baseIdx,
		};
		indices.insert(indices.end(), nodes, nodes + 6);
	}

	void AddTriangle(float3 a, float3 b, float3 c, uint32_t colour) {
		// a c  0 2
		// b    1
		FlatShapeVertex corners[3] = {
			{ a, colour },
			{ b, colour },
			{ c, colour },
		};
		uint16_t baseIdx = (uint16_t)vertices.size();
		vertices.insert(vertices.end(), corners, corners + 3);
		uint16_t nodes[3] = {
			0 + baseIdx, 1 + baseIdx, 2 + baseIdx,
		};
		indices.insert(indices.end(), nodes, nodes + 3);
	}

	void AddRect(float2 low, float2 high, float z, uint32_t colour) {
		AddQuad(
		{ low.x, high.y, z },
		{ low.x, low.y, z },
		{ high.x, low.y, z },
		{ high.x, high.y, z }, colour);
	}

	void AddTrapezoid(float x0, float x1, float base, float y0, float y1, float z, uint32_t colour) {
		AddQuad(
		{ x0, y0, z },
		{ x0, base, z },
		{ x1, base, z },
		{ x1, y1, z }, colour);
	}

	Shape Create() {
		Shape ret;
		ret.vbh = bgfx::createVertexBuffer(Copy(vertices), FlatShapeVertex::decl);
		ret.ibh = bgfx::createIndexBuffer(Copy(indices));
		return ret;
	}
};